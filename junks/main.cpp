
#include <iostream>

#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

int max_width = 103;
int max_height = 64;

bool is_near_rects(const cv::Rect &r1, const cv::Rect &r2)
{
  //  return cv::norm(r1.tl() - r2.tl()) < min_distance;
  // std::cout<<std::abs(r1.tl().y - r2.tl().y)<<"\n";
  // return std::abs(r1.br().y + r2.br().y) < min_distance;
  // auto bounding = get_bounding_rect(r1, r2);
  // return bounding.height <= max_height && bounding.width <= max_width;
  return std::abs(r1.x - r2.x) <= max_width && std::abs(r1.y - r2.y) <= max_height; // equal

  // std::vector<cv::Rect> neighbours;
  // neighbours.push_back(r1);
  // neighbours.push_back(r2);

  // auto bounding = get_bounding_rect(neighbours);

  // return bounding.height <= max_height && bounding.width <= max_width;
}

void sort_contours(std::vector<cv::Rect> &contour_rects)
{

  auto is_more_right = [](auto r1, auto r2)
  { return r1.x < r2.x; };

  auto is_more_up = [](auto r1, auto r2)
  { return r1.y < r2.y; };

  auto is_different_row = [](auto r1, auto r2)
  {
    return std::abs(r1.y - r2.y) > max_height;
  };

  std::sort(contour_rects.begin(), contour_rects.end(), is_more_up);

  auto beg = contour_rects.begin();
  std::vector<cv::Rect>::iterator end;

  do
  {
    end = std::adjacent_find(beg, contour_rects.end(), is_different_row);
    std::sort(beg, (end == contour_rects.end() ? end : end + 1), is_more_right);
    beg = (end == contour_rects.end() ? end : end + 1);

  } while (end != contour_rects.end());
}

cv::Rect get_bounding_rect(const std::vector<cv::Rect> &rects)
{

  auto min_x = std::min_element(rects.begin(), rects.end(), [](auto r1, auto r2)
                                { return r1.x < r2.x; });
  auto min_x_val = min_x->x;
  
  

  auto max_y_tl = std::max_element(rects.begin(), rects.end(), [](auto r1, auto r2)
                                   { return r1.y < r2.y; });
  auto max_y_val_tl = max_y_tl->y;

  auto min_y = std::min_element(rects.begin(), rects.end(), [](auto r1, auto r2)
                                { return r1.y < r2.y; });
  auto min_y_val = min_y->y;

  auto max_x = std::max_element(rects.begin(), rects.end(), [](auto r1, auto r2)
                                { return r1.x + r1.width < r2.x + r2.width; });
  auto max_x_val = max_x->x + max_x->width;

  auto max_y = std::max_element(rects.begin(), rects.end(), [](auto r1, auto r2)
                                { return r1.y + r1.height < r2.y + r2.height; }); //+height

  auto max_y_val = max_y->y + max_y->height;


 // std::cout<<"min_x_val:" << min_x_val << 

  return cv::Rect(cv::Point(min_x_val, min_y->y),
                  cv::Size(max_x_val - min_x_val, max_y_val - min_y_val));
}

std::vector<cv::Rect> getRoisForCharacters(std::vector<std::vector<cv::Point>> &contours)
{

  std::vector<std::pair<cv::Rect, bool>> rois;

  std::transform(contours.begin(), contours.end(), std::back_inserter(rois), [](auto contour)
                 { return std::make_pair(cv::boundingRect(contour), false); });

  std::vector<cv::Rect> reduced_rois;
  
  for (auto it = rois.begin(); it != rois.end(); it++)
  {

    std::cout << it->first.x << " " << it->first.y << " " << it->first.width << " " << it->first.height<< " \n";  
    if (it->second)
      continue;

    std::vector<cv::Rect> near_rects;
    auto iter = it;

    while ((iter = std::find_if(iter, rois.end(), [&it](auto roi2)
                                { return !roi2.second && is_near_rects(it->first, roi2.first); })) != rois.end())
    {
      near_rects.push_back(iter->first);
      iter->second = true;
      iter++;
    }

    if (near_rects.empty())
      near_rects.push_back(it->first);

    cv::Rect character_roi = get_bounding_rect(near_rects);
    near_rects.clear();
    reduced_rois.push_back(character_roi);
  }
  
  std::cout << rois.size() << " reduced: "<<reduced_rois.size() << "\n"; 
  std::cout<<reduced_rois[0].x << " " << reduced_rois[0].y << " "<< reduced_rois[0].width << " " << reduced_rois[0].height<<"\n";
  return reduced_rois;
}

int main()
{
  
  cv::Mat img = cv::imread("Result_103_64.png");
  
  cv::Mat res;
  cv::cvtColor(img, res, cv::COLOR_BGR2GRAY);
  cv::blur(res, res, cv::Size(3, 3));
  cv::Mat canny_output;
  cv::Canny(res, canny_output, 200, 200 * 2);

  std::vector<std::vector<cv::Point>> contours;
  cv::findContours(canny_output, contours, cv::RETR_EXTERNAL, // edge_img
                   cv::CHAIN_APPROX_NONE);

  std::vector<cv::Rect> rois = getRoisForCharacters(contours);

  //log(location, "%s : Reduced number of rois : %u\tNumber of contours : %u\n", get_path().string().c_str(), rois.size(), contours.size());

  sort_contours(rois);
  
   
  
   for(const auto& roi:rois)
  {
    cv::rectangle(img, roi, cv::Scalar(255, 0, 0));

    cv::namedWindow("Test", cv::WINDOW_AUTOSIZE);
    cv::imshow("Test", img);
    // log(location, "Current character:%c\n", i < classes.size() ? classes[i] : '?');

    int key = cv::waitKey(0);
    if (key == 27)
    {
      exit(0);
    }
  }
}

