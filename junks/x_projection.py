import cv2
import numpy as np
import sys

img_name = 'code_word_86'
inputImage = cv2.imread(img_name+'.png')
cv2.imshow("Input",inputImage)

grayImage= cv2.cvtColor(inputImage, cv2.COLOR_BGR2GRAY)
GaussianFilter= cv2.GaussianBlur(grayImage, (5,5), 0)
_, binarizedImage = cv2.threshold(GaussianFilter, 127, 255, cv2.THRESH_BINARY)
print(binarizedImage)

binarizedImage[binarizedImage == 0] = 1
binarizedImage[binarizedImage == 255] = 0
horizontal_projection = np.sum(binarizedImage, axis=0) #0 
#print(horizontal_projection)
height, width = binarizedImage.shape
print('width : ', width)
print('height : ', height)
blankImage = np.zeros((height, width, 3), np.uint8)
dist = sys.maxsize

for row in range(width):
    if dist > cv2.norm((row,0), (row,int(horizontal_projection[row]*width/height))):
        dist = cv2.norm((row,0), (row,int(horizontal_projection[row]*width/height)))
        p1,p2 = (row,0), (row,int(horizontal_projection[row]*width/height))
    print(dist)    
    cv2.line(inputImage, p1,p2, (255,255,255), 1)
print("min" + str(dist))
cv2.imwrite(img_name + "_y_segment.png",inputImage)

cv2.waitKey(0)