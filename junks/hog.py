import cv2
import numpy as np

img = cv2.imread("C:\\image2code\\build\\code_word_3.png")



gx = cv2.Sobel(img,cv2.CV_32F,1,0,ksize=1)
gy = cv2.Sobel(img,cv2.CV_32F,0,1,ksize=1)
mag, angle = cv2.cartToPolar(gx, gy, angleInDegrees=True)

np.set_printoptions(formatter={'float': '{: 0.3f}'.format})
print(angle)


cv2.imwrite("magnitude_i.png",mag)
cv2.imwrite("angle_i.png",angle)
cv2.imwrite("gx_i.png",gx)
cv2.imwrite("gy_i.png",gy)


cv2.waitKey(0)