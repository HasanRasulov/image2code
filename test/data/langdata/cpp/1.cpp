#include "CountVectorizer.hpp"

bool CountVectorizer::skip_word(std::string::iterator beg,
                                std::string::iterator end, size_t min_len)
{
  return (std::distance(beg, end) < min_len) ||
         std::all_of(beg, end, [](auto ch)
                     { return std::isspace(ch); }) ||
         std::count_if(beg, end, [](auto ch)
                       { return !std::isspace(ch); }) <
             min_len;
}



std::multimap<std::string, size_t> CountVectorizer::calculate_frequencies()
{

  std::multimap<std::string, size_t> freqs;

  std::vector<std::string> tokens = get_tokens();

  std::cout << "-----Tokens-----------\n";

  std::for_each(tokens.begin(), tokens.end(), [](const auto &t)
                { std::cout << t << std::endl; });

  // for (const auto &tok : tokens) // vocabulary
  for (size_t i = 0; i < tokens.size() - 1; i++)
  {
    std::string bigram_token = tokens[i] + " " + tokens[i + 1];
    std::cout<< "Bigram="<<bigram_token<<"\n";
    if (freqs.find(bigram_token) == freqs.end())
    {
      auto cnt = std::count(tokens.begin(), tokens.end(), bigram_token);
      freqs.insert({bigram_token, cnt});
    }
  }

  std::transform(freqs.begin(), freqs.end(), std::back_inserter(frequencies), [](const auto &freq)
                 { return freq.second; });

  return freqs;
}

std::vector<size_t> CountVectorizer::get_frequencies() const
{
  return frequencies;
}