#include <gtest/gtest.h>
#include "CharSegmenter.hpp"
#include "Preprocess.hpp"
#include "RGBHashOfImg.hpp"
#include "CountVectorizer.hpp"
#include "PrepareLangData.hpp"
#include "ClassifierIf.hpp"
#include "KNearestClassifier.hpp"
#include "ASCIIHashOfString.hpp"
#include "StringPreProcess.hpp"
#include <algorithm>
#include <utility>
#include <cstdio>

const std::string filename{"../test/data/code.png"};
const std::filesystem::path path_source{"../test/data/1.cpp"};
const std::filesystem::path path_test_file{"../test/data/test.txt"};
bool DRAWROI = true;

TEST(CodeReaderTest, LineSegmentation)
{
    cv::Mat img = cv::imread(filename.c_str(), cv::IMREAD_COLOR);

    cv::imshow("Original", img);

    CharSegmenter seg(img);

    auto lines = seg.getLines();

    ASSERT_EQ(lines.size(), 16);

    for (size_t i = 0; i < lines.size(); i++)
    {
        cv::imwrite(std::filesystem::path{filename}.stem().string() + "_line_" + std::to_string(i + 1) + ".png", lines[i]);

        // cv::imshow(std::string("Line_") + std::to_string(i+1),lines[i]);
    }
    if (cv::waitKey(0) == 27)
    {

        cv::destroyAllWindows();
        exit(0);
    }
}

TEST(CodeReaderTest, WordSegmentation)
{
    cv::Mat img = cv::imread(filename.c_str(), cv::IMREAD_COLOR);

    cv::imshow("Original", img);

    CharSegmenter seg(img);

    auto words = seg.getWords();

    ASSERT_EQ(words.size(), 249); // 100

    for (size_t i = 0; i < words.size(); i++)
    {
        // auto pre = Preprocess::resize(words[i]);

        cv::imwrite(std::filesystem::path{filename}.stem().string() + "_word_" + std::to_string(i + 1) + ".png", words[i]);

        // cv::imshow(std::string("Line_") + std::to_string(i+1),lines[i]);
    }

    if (cv::waitKey(0) == 27)
    {

        cv::destroyAllWindows();
        exit(0);
    }
}

TEST(CodeReaderTest, CharSegmentation)
{
    cv::Mat img = cv::imread(filename.c_str(), cv::IMREAD_UNCHANGED);

    cv::imshow("Original", img);

    CharSegmenter seg(img);

    auto chars = seg.getChars();

    for (size_t i = 0; i < chars.size(); i++)
    {
        // auto pre = Preprocess::resize(words[i]);

        cv::imwrite(std::filesystem::path{filename}.stem().string() + "_char_" + std::to_string(i + 1) + ".png", chars[i]);

        // cv::imshow(std::string("Line_") + std::to_string(i+1),lines[i]);
    }

    ASSERT_EQ(chars.size(), 250); // 249

    if (cv::waitKey(0) == 27)
    {

        cv::destroyAllWindows();
        exit(0);
    }
}

TEST(CodeReaderTest, GET_BOUNDING_RECT)
{

    RGBHashOfImg img("../test/data/Result_34_22.png");

    std::vector<cv::Rect> rects;

    rects.push_back(cv::Rect{100, 50, 10, 10});
    rects.push_back(cv::Rect{100, 80, 10, 20});
    rects.push_back(cv::Rect{120, 60, 10, 30});
    rects.push_back(cv::Rect{110, 100, 20, 10});
    rects.push_back(cv::Rect{100, 100, 30, 70});
    rects.push_back(cv::Rect{115, 60, 10, 20});
    rects.push_back(cv::Rect{10, 80, 30, 10});

    auto b = img.get_bounding_rect(rects);

    ASSERT_EQ(b.x, 10);
    ASSERT_EQ(b.y, 50);
    ASSERT_EQ(b.width, 120);
    ASSERT_EQ(b.height, 120);
}

TEST(CodeReaderTest, GET_TOKENS)
{

    CountVectorizer vec(path_source);

    auto res = vec.get_tokens();

    std::for_each(res.begin(), res.end(), [](auto &t)
                  { std::cout << "\"" << t << "\","; });
    std::cout << "\n";

    std::vector<std::string> expected = {"include", "bool", "CountVectorizer", "skip_word", "std", "string", "iterator", "beg", "std", "string", "iterator", "end", "size_t", "min_len", "return", "std", "distance", "beg", "end", "min_len", "std", "all_of", "beg", "end", "auto", "ch", "return", "std", "isspace", "ch", "std", "count_if", "beg", "end", "auto", "ch", "return", "std", "isspace", "ch", "min_len", "std", "multimap", "std", "string", "size_t", "CountVectorizer", "calculate_frequencies", "std", "multimap", "std", "string", "size_t", "freqs", "std", "vector", "std", "string", "tokens", "get_tokens", "std", "cout", "std", "for_each", "tokens", "begin", "tokens", "end", "const", "auto", "std", "cout", "std", "endl", "for", "size_t", "tokens", "size", "std", "string", "bigram_token", "tokens", "tokens", "std", "cout", "bigram_token", "if", "freqs", "find", "bigram_token", "freqs", "end", "auto", "cnt", "std", "count", "tokens", "begin", "tokens", "end", "bigram_token", "freqs", "insert", "bigram_token", "cnt", "std", "transform", "freqs", "begin", "freqs", "end", "std", "back_inserter", "frequencies", "const", "auto", "freq", "return", "freq", "second", "return", "freqs", "std", "vector", "size_t", "CountVectorizer", "get_frequencies", "const", "return", "frequencies"};
    ASSERT_EQ(res.size(), expected.size());

    ASSERT_TRUE(std::equal(res.begin(), res.end(), expected.begin()));
}

TEST(CodeReaderTest, GET_GRAMS)
{
    CountVectorizer vec(path_source);

    auto res = vec.get_ngrams(1);
    auto tokens = vec.get_tokens();

    ASSERT_EQ(res.size(), tokens.size());

    ASSERT_TRUE(std::equal(res.begin(), res.end(), tokens.begin()));
}

TEST(CodeReaderTest, GET_BIGRAMS)
{
    CountVectorizer vec(path_source);

    auto res = vec.get_ngrams();

    std::for_each(res.begin(), res.end(), [](auto &s)
                  { std::cout << s << "\n"; });

    std::vector<std::string> expected = {"include bool", "bool CountVectorizer", "CountVectorizer skip_word", "skip_word std", "std string", "string iterator", "iterator beg", "beg std", "std string", "string iterator", "iterator end", "end size_t", "size_t min_len", "min_len return", "return std", "std distance", "distance beg", "beg end", "end min_len", "min_len std", "std all_of", "all_of beg", "beg end", "end auto", "auto ch", "ch return", "return std", "std isspace", "isspace ch", "ch std", "std count_if", "count_if beg", "beg end", "end auto", "auto ch", "ch return", "return std", "std isspace", "isspace ch", "ch min_len", "min_len std", "std multimap", "multimap std", "std string", "string size_t", "size_t CountVectorizer", "CountVectorizer calculate_frequencies", "calculate_frequencies std", "std multimap", "multimap std", "std string", "string size_t", "size_t freqs", "freqs std", "std vector", "vector std", "std string", "string tokens", "tokens get_tokens", "get_tokens std", "std cout", "cout std", "std for_each", "for_each tokens", "tokens begin", "begin tokens", "tokens end", "end const", "const auto", "auto std", "std cout", "cout std", "std endl", "endl for", "for size_t", "size_t tokens", "tokens size", "size std", "std string", "string bigram_token", "bigram_token tokens", "tokens tokens", "tokens std", "std cout", "cout bigram_token", "bigram_token if", "if freqs", "freqs find", "find bigram_token", "bigram_token freqs", "freqs end", "end auto", "auto cnt", "cnt std", "std count", "count tokens", "tokens begin", "begin tokens", "tokens end", "end bigram_token", "bigram_token freqs", "freqs insert", "insert bigram_token", "bigram_token cnt", "cnt std", "std transform", "transform freqs", "freqs begin", "begin freqs", "freqs end", "end std", "std back_inserter", "back_inserter frequencies", "frequencies const", "const auto", "auto freq", "freq return", "return freq", "freq second", "second return", "return freqs", "freqs std", "std vector", "vector size_t", "size_t CountVectorizer", "CountVectorizer get_frequencies", "get_frequencies const", "const return", "return frequencies"};
    ASSERT_EQ(res.size(), expected.size());

    ASSERT_TRUE(std::equal(res.begin(), res.end(), expected.begin()));
}

TEST(CodeReaderTest, GET_TRIGRAMS)
{
    CountVectorizer vec(path_source);

    auto res = vec.get_ngrams(3);

    std::vector<std::string> expected = {"include bool CountVectorizer", "bool CountVectorizer skip_word", "CountVectorizer skip_word std", "skip_word std string", "std string iterator", "string iterator beg", "iterator beg std", "beg std string", "std string iterator", "string iterator end", "iterator end size_t", "end size_t min_len", "size_t min_len return", "min_len return std", "return std distance", "std distance beg", "distance beg end", "beg end min_len", "end min_len std", "min_len std all_of", "std all_of beg", "all_of beg end", "beg end auto", "end auto ch", "auto ch return", "ch return std", "return std isspace", "std isspace ch", "isspace ch std", "ch std count_if", "std count_if beg", "count_if beg end", "beg end auto", "end auto ch", "auto ch return", "ch return std", "return std isspace", "std isspace ch", "isspace ch min_len", "ch min_len std", "min_len std multimap", "std multimap std", "multimap std string", "std string size_t", "string size_t CountVectorizer", "size_t CountVectorizer calculate_frequencies", "CountVectorizer calculate_frequencies std", "calculate_frequencies std multimap", "std multimap std", "multimap std string", "std string size_t", "string size_t freqs", "size_t freqs std", "freqs std vector", "std vector std", "vector std string", "std string tokens", "string tokens get_tokens", "tokens get_tokens std", "get_tokens std cout", "std cout std", "cout std for_each", "std for_each tokens", "for_each tokens begin", "tokens begin tokens", "begin tokens end", "tokens end const", "end const auto", "const auto std", "auto std cout", "std cout std", "cout std endl", "std endl for", "endl for size_t", "for size_t tokens", "size_t tokens size", "tokens size std", "size std string", "std string bigram_token", "string bigram_token tokens", "bigram_token tokens tokens", "tokens tokens std", "tokens std cout", "std cout bigram_token", "cout bigram_token if", "bigram_token if freqs", "if freqs find", "freqs find bigram_token", "find bigram_token freqs", "bigram_token freqs end", "freqs end auto", "end auto cnt", "auto cnt std", "cnt std count", "std count tokens", "count tokens begin", "tokens begin tokens", "begin tokens end", "tokens end bigram_token", "end bigram_token freqs", "bigram_token freqs insert", "freqs insert bigram_token", "insert bigram_token cnt", "bigram_token cnt std", "cnt std transform", "std transform freqs", "transform freqs begin", "freqs begin freqs", "begin freqs end", "freqs end std", "end std back_inserter", "std back_inserter frequencies", "back_inserter frequencies const", "frequencies const auto", "const auto freq", "auto freq return", "freq return freq", "return freq second", "freq second return", "second return freqs", "return freqs std", "freqs std vector", "std vector size_t", "vector size_t CountVectorizer", "size_t CountVectorizer get_frequencies", "CountVectorizer get_frequencies const", "get_frequencies const return", "const return frequencies"};
    ASSERT_EQ(res.size(), expected.size());

    ASSERT_TRUE(std::equal(res.begin(), res.end(), expected.begin()));
}
TEST(CodeReaderTest, CALCULATE_FREQUENCIES)
{
    CountVectorizer vec(path_source);

    auto res = vec.calculate_frequencies();
    std::for_each(res.begin(), res.end(), [](auto &p)
                  { std::cout << p.first << "  " << p.second << std::endl; });
    auto freq = vec.get_frequencies();

    std::vector<size_t> expected = {1, 1, 1, 1, 2, 1, 1, 1, 1, 3, 1, 1, 2, 1, 2, 1, 1, 1, 1, 1, 2, 1, 2, 2, 1, 1, 1, 1, 2, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 2, 2, 1, 1, 1, 3, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 2, 2, 6, 1, 2, 1, 2, 2, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1};
    ASSERT_EQ(freq.size(), expected.size());

    ASSERT_TRUE(std::equal(freq.begin(), freq.end(), expected.begin()));
    std::for_each(freq.begin(), freq.end(), [](auto &p)
                  { std::cout << p << ","; });
    std::cout << std::endl;
}
// WRITE FOR NUMBER OF TOKENS LESS THAN GRAMs
TEST(CodeReaderTest, SAVE_AND_LOAD_VOCABULARY)
{

    PrepareLangData lang(path_source, path_source);

    std::vector<std::string> voc = {"include bool", "bool CountVectorizer", "CountVectorizer skip_word", "skip_word std", "std string", "string iterator", "iterator beg", "beg std", "std string", "string iterator", "iterator end", "end size_t", "size_t min_len", "min_len return", "return std", "std distance", "distance beg", "beg end", "end min_len", "min_len std", "std all_of", "all_of beg", "beg end", "end auto", "auto ch", "ch return", "return std", "std isspace", "isspace ch", "ch std", "std count_if", "count_if beg", "beg end", "end auto", "auto ch", "ch return", "return std", "std isspace", "isspace ch", "ch min_len", "min_len std", "std multimap", "multimap std", "std string", "string size_t", "size_t CountVectorizer", "CountVectorizer calculate_frequencies", "calculate_frequencies std", "std multimap", "multimap std", "std string", "string size_t", "size_t freqs", "freqs std", "std vector", "vector std", "std string", "string tokens", "tokens get_tokens", "get_tokens std", "std cout", "cout std", "std for_each", "for_each tokens", "tokens begin", "begin tokens", "tokens end", "end const", "const auto", "auto std", "std cout", "cout std", "std endl", "endl for", "for size_t", "size_t tokens", "tokens size", "size std", "std string", "string bigram_token", "bigram_token tokens", "tokens tokens", "tokens std", "std cout", "cout bigram_token", "bigram_token if", "if freqs", "freqs find", "find bigram_token", "bigram_token freqs", "freqs end", "end auto", "auto cnt", "cnt std", "std count", "count tokens", "tokens begin", "begin tokens", "tokens end", "end bigram_token", "bigram_token freqs", "freqs insert", "insert bigram_token", "bigram_token cnt", "cnt std", "std transform", "transform freqs", "freqs begin", "begin freqs", "freqs end", "end std", "std back_inserter", "back_inserter frequencies", "frequencies const", "const auto", "auto freq", "freq return", "return freq", "freq second", "second return", "return freqs", "freqs std", "std vector", "vector size_t", "size_t CountVectorizer", "CountVectorizer get_frequencies", "get_frequencies const", "const return", "return frequencies"};

    std::pair<std::string, std::set<std::string>> set_voc;
    std::copy(voc.begin(), voc.end(), std::inserter(set_voc.second, set_voc.second.end()));
    set_voc.first = "cpp";

    lang.save_vocabulary(set_voc);

    std::map<std::string, std::set<std::string>> loaded_voc = PrepareLangData::load_vocabulary(path_source);

    ASSERT_EQ(set_voc.second.size(), loaded_voc.begin()->second.size());

    std::cout << "\n--------------------------------------------------Saved  Vocabulary----------------------------------------------------------------------------------------------------\n";
    std::cout << std::endl;
    std::for_each(set_voc.second.begin(), set_voc.second.end(), [](auto &p)
                  { std::cout << p << " "; });
    std::cout << "\n--------------------------------------------------Loaded Vocabulary----------------------------------------------------------------------------------------------------\n";
    std::for_each(loaded_voc.begin()->second.begin(), loaded_voc.begin()->second.end(), [](auto &p)
                  { std::cout << p << " "; });

    ASSERT_TRUE(std::equal(set_voc.second.begin(), set_voc.second.end(), loaded_voc.begin()->second.begin(), loaded_voc.begin()->second.end()));

    remove(("vocabulary_" + set_voc.first + ".data").c_str());
}

TEST(CodeReaderTest, PrepareLangData)
{
    remove("../test/data/langdata_cpp.csv");
    remove("../test/data/langdata_java.csv");
    remove("../test/data/langdata_py.csv");

    PrepareLangData pre("../test/data/langdata", "../test/data/langdata.csv");

    auto max = pre.save_to_csv(false);
    std::set vocabulary_expected_cpp = {"CountVectorizer calculate_frequencies", "CountVectorizer get_frequencies", "CountVectorizer skip_word", "all_of beg", "auto ch", "auto cnt", "auto freq", "auto std", "back_inserter frequencies", "beg end", "beg std", "begin freqs", "begin tokens", "bigram_token cnt", "bigram_token freqs", "bigram_token if", "bigram_token tokens", "bool CountVectorizer", "calculate_frequencies std", "ch min_len", "ch return", "ch std", "cnt std", "const auto", "const return", "count tokens", "count_if beg", "cout bigram_token", "cout std", "distance beg", "end auto", "end bigram_token", "end const", "end min_len", "end size_t", "end std", "endl for", "find bigram_token", "for size_t", "for_each tokens", "freq return", "freq second", "freqs begin", "freqs end", "freqs find", "freqs insert", "freqs std", "frequencies const", "get_frequencies const", "get_tokens std", "if freqs", "include bool", "insert bigram_token", "isspace ch", "iterator beg", "iterator end", "min_len return", "min_len std", "multimap std", "return freq", "return freqs", "return frequencies", "return std", "second return", "size std", "size_t CountVectorizer", "size_t freqs", "size_t min_len", "size_t tokens", "skip_word std", "std all_of", "std back_inserter", "std count", "std count_if", "std cout", "std distance", "std endl", "std for_each", "std isspace", "std multimap", "std string", "std transform", "std vector", "string bigram_token", "string iterator", "string size_t", "string tokens", "tokens begin", "tokens end", "tokens get_tokens", "tokens size", "tokens std", "tokens tokens", "transform freqs", "vector size_t", "vector std"};
    std::set vocabulary_expected_java = {"@Override public", "AbstractDocument Map", "AbstractDocument implements", "Collection import", "Collection stream", "Document private", "Function Map", "Function import", "List Map", "List import", "Map String", "Map import", "Object constructor", "Object el", "Object get", "Object properties", "Object value", "Objects import", "Objects nonNull", "Objects requireNonNull", "Stream children", "Stream ofNullable", "Stream public", "String Object", "String key", "String toString", "StringBuilder builder", "Void put", "abstract class", "abstractdocument import", "append append", "append builder", "append getClass", "append key", "append properties", "append return", "append value", "builder append", "builder new", "builder toString", "children String", "class AbstractDocument", "com iluwatar", "constructor @Override", "constructor return", "el List", "el findAny", "filter Objects", "final Map", "findAny stream", "flatMap Collection", "forEach key", "function Function", "get String", "get key", "getClass getName", "getName append", "iluwatar abstractdocument", "implements Document", "import java", "java util", "key @Override", "key Function", "key Object", "key append", "key filter", "key return", "key value", "map constructor", "map el", "new StringBuilder", "nonNull map", "null @Override", "ofNullable get", "package com", "private final", "properties @Override", "properties Objects", "properties forEach", "properties get", "properties properties", "properties protected", "properties put", "properties this", "protected AbstractDocument", "public Object", "public Stream", "public String", "public Void", "public abstract", "put String", "put key", "requireNonNull properties", "return Stream", "return builder", "return null", "return properties", "stream Stream", "stream flatMap", "stream map", "this properties", "toString var", "util Collection", "util List", "util Map", "util Objects", "util function", "util stream", "value append", "value builder", "value properties", "value return", "var builder"};
    std::set vocabulary_expected_py = {"BytesIO base", "BytesIOimport requestsfrom", "Exception def", "Image open", "Imageclass ServiceError", "PIL import", "ServiceError Exception", "ServiceError status_code", "__init__ self", "backend_url json", "backend_url requests", "base bdecode", "basefrom io", "bdecode img", "def __init__", "def get_model_version", "else raise", "for img", "get return", "get url", "get_images_from_backend prompt", "get_model_version url", "if status_code", "images Image", "images json", "images version", "img for", "img in", "import BytesIOimport", "import Imageclass", "import basefrom", "in images", "io import", "json get", "json images", "json json", "json prompt", "json return", "open BytesIO", "post backend_url", "prompt backend_url", "prompt if", "raise ServiceError", "requests get", "requests post", "requestsfrom PIL", "return images", "return version", "self status_code", "status_code def", "status_code json", "status_code self", "status_code status_codedef", "status_code version", "status_codedef get_images_from_backend", "url if", "url requests", "version else", "version json"};
    std::vector<double> features_cpp = {0.7615, 0.7615, 0.7615, 0.7615, 1.20695, 0.7615, 0.7615, 0.7615, 0.7615, 1.523, 0.7615, 0.7615, 1.20695, 0.7615, 1.20695, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 1.20695, 0.7615, 1.20695, 1.20695, 0.7615, 0.7615, 0.7615, 0.7615, 1.20695, 0.7615, 1.523, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 1.20695, 0.7615, 0.7615, 1.20695, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 1.20695, 0.7615, 0.7615, 0.7615, 1.20695, 1.20695, 0.7615, 0.7615, 0.7615, 1.523, 0.7615, 0.7615, 1.20695, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 1.523, 0.7615, 0.7615, 0.7615, 1.20695, 1.20695, 2.1378, 0.7615, 1.20695, 0.7615, 1.20695, 1.20695, 0.7615, 1.20695, 1.20695, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615};
    std::vector<double> features_java = {1.76815, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 1.76815, 0.7615, 0.7615, 0.7615, 0.7615, 1.20695, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 1.76815, 1.523, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 1.20695, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 1.523, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 1.20695, 0.7615, 0.7615, 0.7615, 0.7615, 2.1378, 2.1378, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 1.20695, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615};
    std::vector<double> features_py = {0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 1.20695, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 1.20695, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 1.20695, 0.7615, 0.7615, 1.20695, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 1.20695, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 1.20695, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 1.20695, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 0.7615, 1.20695, 1.20695};
    auto voc = PrepareLangData::load_vocabulary("../test/data/langdata.csv");

    ASSERT_EQ(voc.size(), HashIf::lang_classes.size());

    CumulativeFrequencyAdditionClassifier cls("../test/data/langdata.csv");
    auto number_of_variables = cls.read_data(static_cast<HashIf::converter>(HashIf::getClass));

    for (const auto l : HashIf::lang_classes)
    {
        ASSERT_TRUE(cls.entries.contains(l.second));
    }

    ASSERT_EQ(cls.entries.size(), HashIf::lang_classes.size());
    ASSERT_EQ(cls.entries["cpp"].size(), 1);
    ASSERT_EQ(cls.entries["java"].size(), 1);
    ASSERT_EQ(cls.entries["py"].size(), 1);

    auto &features = cls.entries["cpp"][0];
    ASSERT_TRUE(std::equal(features_cpp.begin(), features_cpp.end(), features.begin(), features.end()));
    features.clear();

    features = cls.entries["java"][0];
    ASSERT_TRUE(std::equal(features_java.begin(), features_java.end(), features.begin(), features.end()));
    features.clear();

    features = cls.entries["py"][0];
    ASSERT_TRUE(std::equal(features_py.begin(), features_py.end(), features.begin(), features.end()));
}

TEST(CodeReaderTest, NgramHashOfFile)
{
    remove("../test/data/langdata_cpp.csv");
    remove("../test/data/langdata_java.csv");
    remove("../test/data/langdata_py.csv");

    PrepareLangData pre("../test/data/langdata", "../test/data/langdata.csv");
    pre.save_to_csv(false);
    CumulativeFrequencyAdditionClassifier cls("../test/data/langdata.csv");
    auto number_of_variables = cls.read_data(static_cast<HashIf::converter>(HashIf::getClass));
    auto freqs_sum = cls.get_local_frequencies_accumulation();
    auto feature_vec = ASCIIHashOfString::calculate_hash_sample("../test/data/test.txt", "../test/data/langdata.csv", freqs_sum);
    std::vector<double> expected_feature = {-1,  0.7615,  0.7615,  0.7615,  0.7615,  1.20695,  0.7615,  0.7615,  0.7615,  0.7615,  0.7615,  1.20695,  1.20695,  0.7615,  0.7615,  1.523,  0.7615,  0.7615,  0.7615,  0.7615,  1.523,  0.7615,  0.7615,  2.1378,  0.7615,  1.20695,  0.7615,  -2,  -3};

    ASSERT_GE(feature_vec.size(), 2); // because of class indications
    ASSERT_EQ(feature_vec.size(), expected_feature.size());
    ASSERT_TRUE(std::equal(feature_vec.begin(), feature_vec.end(), expected_feature.begin(), expected_feature.end()));

    std::for_each(feature_vec.begin(), feature_vec.end(), [](auto s)
                  { std::cout << s << ","; });
    std::cout << std::endl;
}

TEST(CodeReaderTest, Remove_comments)
{
    StringPreProcess pre(path_test_file);

    std::ifstream file("../test/data/remove_comment.result");

    std::string expected{std::istreambuf_iterator<char>(file),
                         std::istreambuf_iterator<char>()};

    file.close();

    pre.setBlockCommentChars(std::make_pair("/*", "*/"));
    pre.setSingleLineCommentChar("//");

    pre.remove_comments();

    std::string result;
    std::cout << expected << "\n";
    /*EXPECT_PRED3([](auto str, auto s1, auto s2)
                 { return str == s1 || str == s2; },
                 result, pre.getContent(), expected);*/
}
// doesn't work
TEST(CodeReaderTest, Remove_strings)
{

    StringPreProcess pre(path_test_file);

    std::ifstream file("../test/data/remove_string.result");

    std::string expected{std::istreambuf_iterator<char>(file),
                         std::istreambuf_iterator<char>()};

    file.close();

    pre.setStringChars(std::vector{std::string("\"")});

    pre.remove_strings();

    std::string result;
    std::cout << expected << "\n";
    /*EXPECT_PRED3([](auto str, auto s1, auto s2)
                 { return str == s1 || str == s2; },
                 result, pre.getContent(), expected);*/
}
// does not work
TEST(CodeReaderTest, Remove_literals)
{
    StringPreProcess pre(path_test_file);

    std::ifstream file("../test/data/remove_literal.result");

    std::string expected{std::istreambuf_iterator<char>(file),
                         std::istreambuf_iterator<char>()};

    file.close();

    pre.remove_literals();

    std::string result;
    std::cout << expected << "\n";
    /*EXPECT_PRED3([](auto str, auto s1, auto s2)
                 { return str == s1 || str == s2; },
                 result, pre.getContent(), expected);*/
}

TEST(CodeReaderTest, Remove_punctuations)
{
    StringPreProcess pre(path_test_file);

    std::ifstream file("../test/data/remove_punctuation.result");

    std::string expected{std::istreambuf_iterator<char>(file),
                         std::istreambuf_iterator<char>()};

    file.close();

    pre.remove_punctuations();

    std::string result;
    std::cout << expected << "\n";
    /*EXPECT_PRED3([](auto str, auto s1, auto s2)
                 { return str == s1 || str == s2; },
                 result, pre.getContent(), expected);*/
}