#pragma once

#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include "Preprocess.hpp"
#include "Logger.hpp"
#include "CharSegmenter.hpp"

#include <algorithm>
#include <iterator>
#include <filesystem>

using namespace std::string_literals;
using namespace std::placeholders;

extern bool DRAWROI;

enum class Classes : unsigned
{
  UNKNOWN = 0,
  CPP = 1,
  JAVA = 2,
  PY = 3

};

class HashIf
{

public:
  std::filesystem::path path;

  size_t max_width;
  size_t max_height;
  static inline const std::map<Classes, std::string> lang_classes = {{Classes::CPP, "cpp"}, {Classes::JAVA, "java"}, {Classes::PY, "py"}};
  static inline constexpr std::array classes = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                                                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                                                'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
                                                'U', 'V', 'W', 'X', 'Y', 'Z', '!', '\"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '<', '=', '>', '?', '@', '[', '\\', ']', '^', '_', '`', '{', '|', '}', '~', '?'};

  using converter = int (*)(const std::string &);
  HashIf(std::filesystem::path path) { this->path = path; }
  inline auto get_path() const { return path; }
  virtual void calculate_hash() = 0;
  static int getClass(const std::string &);
  static std::string getClass(Classes);
  bool is_near_rects(const cv::Rect &, const cv::Rect &);
  bool is_inside(const cv::Rect &r1, const cv::Rect &r2);
  virtual void add_padding(size_t);
  cv::Rect get_bounding_rect(const std::vector<cv::Rect> &);

protected:
  void draw_rois(cv::Mat &, const std::vector<cv::Rect> &);

  void sort_contours(std::vector<cv::Rect> &);
  virtual std::vector<cv::Mat> getRoisForCharacters(cv::Mat);
  virtual std::vector<cv::Rect> getRoisForCharacters(std::vector<std::vector<cv::Point>> &, std::function<bool(const cv::Rect &, const cv::Rect &)>);
};
