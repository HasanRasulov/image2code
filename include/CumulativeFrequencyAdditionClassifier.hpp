#pragma once
#include "ClassifierIf.hpp"
#include "Logger.hpp"
#include "ASCIIHashOfString.hpp"
#include <limits>
#include <map>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cmath>

class CumulativeFrequencyAdditionClassifier : public ClassifierIf
{

  std::map<std::filesystem::path, std::vector<double>> test_entries;
  size_t number_of_test_files;
  static inline const std::filesystem::path path_to_test_sources = "../test/data/langdatatest";
public:
  std::map<std::string, std::vector<std::vector<double>>> entries;
  CumulativeFrequencyAdditionClassifier(std::string);
  ~CumulativeFrequencyAdditionClassifier();
  std::pair<bool, size_t> read_data(HashIf::converter) override;
  size_t prepare_train_and_test_data(const float ratio = DIV_RATIO) override;

  std::map<std::string, std::vector<double>> get_local_frequencies_accumulation();

  void train_and_test_classfier() override;
  float predict(const cv::Mat &) override;
  std::pair<bool, size_t> load() override;
};
