#pragma once

#include "ImageReaderIf.hpp"
#include "SVMClassifier.hpp"
#include "PrepareImgData.hpp"
#include "Logger.hpp"
#include "KNearestClassifier.hpp"
#include "BayesClassifier.hpp"

#include "RGBHashOfImg.hpp"
#include "HOGHashOfImg.hpp"
#include "OutputCorrecter.hpp"
#include "Preprocess.hpp"
#include "CharSegmenter.hpp"

class ImageReaderCustom : public ImageReaderIf
{

  ClassifierIf *classifier;

public:
  ImageReaderCustom(std::initializer_list<std::filesystem::path> paths, std::filesystem::path result)
      : ImageReaderIf{paths, result}, classifier{nullptr} {}

  void image2code() override;

  ~ImageReaderCustom()
  {
    delete classifier;
  }

  void setClassifier(ClassifierIf *clsfr);
};
