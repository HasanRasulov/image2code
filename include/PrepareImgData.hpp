#pragma once

#include "ThreadPool.hpp"
#include "RGBHashOfImg.hpp"
#include "PrepareDataIf.hpp"
#include "HOGHashOfImg.hpp"
#include "Logger.hpp"
#include <fstream>

class PrepareImgData : public PrepareDataIf
{

public:
  PrepareImgData(std::filesystem::path path, std::filesystem::path save_to);
  uint16_t save_to_csv(bool padding) const;
};
