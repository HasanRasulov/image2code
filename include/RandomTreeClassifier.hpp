#pragma once
#include "ClassifierIf.hpp"

class RandomTreeClassifier : public ClassifierIf
{

  cv::Ptr<cv::ml::RTrees> model;
  cv::TermCriteria TC(int iters, double eps);

public:
  RandomTreeClassifier(std::string);
  ~RandomTreeClassifier();

  void train_and_test_classfier() override;

  float predict(const cv::Mat &) override;
  std::pair<bool, size_t> load() override;
};
