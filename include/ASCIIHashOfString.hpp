#ifndef ASCIIHASHIFSTRING__HPP
#define ASCIIHASHIFSTRING__HPP

#include "HashIf.hpp"
#include "StringPreProcess.hpp"
#include "CountVectorizer.hpp"
#include "Logger.hpp"
#include "ClassifierIf.hpp"
#include "CumulativeFrequencyAdditionClassifier.hpp"
#include "PrepareLangData.hpp"
#include <fstream>
#include <map>
#include <set>
#include <numeric>
#include <vector>
#include <tuple>

class ASCIIHashOfString : public HashIf
{

  using Specs = std::tuple<std::string, std::pair<std::string, std::string>, std::vector<std::string>>;

  static inline const std::map<Classes, Specs> lang_chars = {
      {Classes::CPP, std::make_tuple("//"s, std::make_pair("/*"s, "*/"s), std::vector<std::string>{"\""s, "\'"s})},
      {Classes::JAVA, std::make_tuple("//"s, std::make_pair("/*"s, "*/"s), std::vector<std::string>{"\""s})},
      {Classes::PY, std::make_tuple("#"s, std::make_pair(""s, ""s), std::vector<std::string>{"\""s, "\'", "\'''"})}};

public:
  ASCIIHashOfString(std::filesystem::path path) : HashIf(path) {}

  friend std::ostream &operator<<(std::ostream &, const ASCIIHashOfString &);
  std::multimap<std::string, size_t> hash;
  std::string class_name;
  void calculate_hash() override;

  std::vector<size_t> get_hash() const;
  static std::vector<double> calculate_hash_sample(std::string, std::string, std::map<std::string, std::vector<double>> &);
};

#endif