#ifndef __PREPARELANGDATA__HPP
#define __PREPARELANGDATA__HPP

#include "ASCIIHashOfString.hpp"
#include "PrepareDataIf.hpp"
#include "ThreadPool.hpp"
#include "Logger.hpp"
#include "HashIf.hpp"
#include <set>
#include <math.h>

class PrepareLangData : public PrepareDataIf
{

   static inline char WORD_DELIM = ',';

public:
   static inline std::string vocabulary_file_prefix = "vocabulary";

   PrepareLangData(std::filesystem::path path, std::filesystem::path save_to);
   uint16_t save_to_csv(bool padding) const override;
   void save_vocabulary(const std::pair<std::string, std::set<std::string>> &) const;

   static std::map<std::string, std::set<std::string>> load_vocabulary(std::filesystem::path);
};

#endif