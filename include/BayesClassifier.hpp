#pragma once
#include "ClassifierIf.hpp"

class BayesClassifier : public ClassifierIf
{

  cv::Ptr<cv::ml::NormalBayesClassifier> model;

public:
  BayesClassifier(std::string);
  ~BayesClassifier();

  void train_and_test_classfier() override;

  float predict(const cv::Mat &) override;
  std::pair<bool, size_t> load() override;
};
