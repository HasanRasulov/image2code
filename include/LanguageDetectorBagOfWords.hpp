#ifndef __LANGUAGE__DETECTOR__BAG__WORDS__HPP__
#define __LANGUAGE__DETECTOR__BAG__WORDS__HPP__

#include "LanguageDetectorIf.hpp"
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

#include "ASCIIHashOfString.hpp"
#include "BayesClassifier.hpp"
#include "KNearestClassifier.hpp"
#include "RandomTreeClassifier.hpp"
#include "SVMClassifier.hpp"
#include "CumulativeFrequencyAdditionClassifier.hpp"
#include "Logger.hpp"

class LanguageDetectorBagOfWords : public LanguageDetectorIf
{

public:
  Classes detectLanguage(std::string) override;
  Classes detectLanguage(std::filesystem::path) override;
};

#endif