#ifndef __CHARSEGMENTER_HPP__
#define __CHARSEGMENTER_HPP__

#include <vector>
#include <utility>
#include <iostream>
#include <algorithm>
#include <source_location>

#include "Logger.hpp"
#include "Preprocess.hpp"

#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

class CharSegmenter
{

    cv::Mat img;
    cv::Mat original;

public:
    CharSegmenter(cv::Mat sample_img) : original{sample_img.clone()}
    {
        img = Preprocess::preprocess(sample_img);
    };

    std::vector<cv::Mat> getLines();
    std::vector<std::vector<std::pair<cv::Point, cv::Point>>> getLineSegments();

    std::vector<cv::Mat> getWords();
    std::vector<std::vector<std::pair<cv::Point, cv::Point>>> getWordSegments(cv::Mat);

    std::vector<cv::Mat> getChars();
    std::vector<std::vector<std::pair<cv::Point, cv::Point>>> getCharSegments(cv::Mat);
};

#endif