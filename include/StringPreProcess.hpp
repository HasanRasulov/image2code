#pragma once

#include "ASCIIHashOfString.hpp"
#include <iostream>
#include <iterator>
#include <vector>
#include <string_view>
using namespace std::string_literals;

class StringPreProcess
{

  std::string content;

  std::pair<std::string, std::string> block_comment_chars;
  std::string single_line_comment_char;

  std::vector<std::string> string_chars;

  void remove_chars_between(const std::string &, const std::string &);

  std::string::const_iterator find_str(const std::string &str,
                                       const std::string &search,
                                       std::string::const_iterator begin);

public:
  StringPreProcess(std::filesystem::path path_to_source);
  StringPreProcess(std::string &content) : content{std::move(content)} {}
  StringPreProcess(const StringPreProcess &) = delete;
  StringPreProcess(StringPreProcess &&) = delete;
  StringPreProcess &operator=(const StringPreProcess &) = delete;
  StringPreProcess &operator=(StringPreProcess &&) = delete;

#ifdef __linux__
  static inline constexpr std::string_view newLine = "\n";
#elif _WIN32
  static inline constexpr std::string_view newLine = "\n"; // \r\n
#endif

  void setBlockCommentChars(std::pair<std::string, std::string>);
  void setSingleLineCommentChar(std::string);
  void setStringChars(std::vector<std::string> chars);

  StringPreProcess &remove_blank_lines();

  StringPreProcess &remove_comments();

  StringPreProcess &remove_strings();

  StringPreProcess &remove_punctuations();

  StringPreProcess &remove_literals();

  std::string getContent() { return content; }
};
