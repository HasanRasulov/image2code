#pragma once

#include "ImageReaderIf.hpp"
#include <leptonica/allheaders.h>
#include <tesseract/baseapi.h>

class ImageReaderTesseract : public ImageReaderIf
{

public:
  ImageReaderTesseract(std::initializer_list<std::filesystem::path> paths, std::filesystem::path result)
      : ImageReaderIf{paths, result} {}

  void image2code() override;
};
