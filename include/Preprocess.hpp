
#pragma once

#include <opencv2/opencv.hpp>
#include "Logger.hpp"

class Preprocess
{

public:
  static inline constexpr size_t WIDTH_HOG = 25;
  static inline constexpr size_t HEIGHT_HOG = 50;
  static constexpr float THRESHOLD = .8f;
  static constexpr std::pair<uint8_t, uint8_t> BLACK{0U, 100U};
  static constexpr std::pair<uint8_t, uint8_t> WHITE{250U, 255U};

  static cv::Mat preprocess(cv::Mat);
  static cv::Mat resize(const cv::Mat);
  static cv::Mat deskew(cv::Mat &);
  static bool isSameColourRow(const std::vector<uint8_t> &, size_t, float threshold = THRESHOLD, uint8_t colour = WHITE.second);
  static cv::Mat remove_begining_trailing_background(const cv::Mat &src, float threshold, uint8_t colour);
};
