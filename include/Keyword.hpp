#ifndef __KEYWORD__HPP__
#define __KEYWORD__HPP__

#include <string>
#include <ostream>

class Keyword
{

    std::string actual_keyword;
    float confidence_value;

public:
    Keyword(std::string keyword, float confidence_val) : actual_keyword{keyword}, confidence_value{confidence_val} {}
    Keyword() = default;
    ~Keyword() = default;

    friend std::ostream &operator<<(std::ostream &, const Keyword &);
    std::string getKeyword() const;
    float getConfidence() const;
    void setKeyword(std::string kyw);
    void setConfidence(float val);
};

#endif