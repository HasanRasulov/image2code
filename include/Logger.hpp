#pragma once
#include <string>
#include <cstdio>
#include <iostream>
#include <source_location>
#include <filesystem>

enum class LogLevel
{
    INFO,
    WARNNING,
    ERROR
};

template <LogLevel level = LogLevel::INFO, typename... T>
static void log(const std::source_location &location, std::string format, T &&...t)
{
    std::filesystem::path filename{location.file_name()};
    std::string prototype{location.function_name()};
    auto pos_of_func_name = prototype.find(":") + 2;
    std::string func_name{prototype.substr(pos_of_func_name, prototype.find("(") - pos_of_func_name)};

    if constexpr (level == LogLevel::ERROR || level == LogLevel::WARNNING || level == LogLevel::INFO)
    {
        std::cout << filename.filename() << ":"
                  << location.line() << ":"
                  << func_name << ": " << std::flush;

        fprintf(stdout, format.c_str(), t...);
    }
}
