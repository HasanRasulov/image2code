#pragma once
#include "HashIf.hpp"
#include "Logger.hpp"

#include "opencv2/core.hpp"
#include "opencv2/ml.hpp"

#include <algorithm>
#include <fstream>
#include <functional>
#include <string>
#include <iterator>
#include <utility>

using namespace std::string_literals;

class ClassifierIf
{

protected:
  constexpr static inline float DIV_RATIO = 0.8f;

  std::vector<std::string> split(const std::string &, char);
  std::string::iterator read_class_name(std::string &);
  std::string get_classifier_name(const std::source_location &);

public:
  cv::Mat data;
  cv::Mat responses;
  cv::Ptr<cv::ml::TrainData> train_data;
  std::string filename_to_save;
  std::string filename_to_read;

  virtual std::pair<bool, size_t> read_data(HashIf::converter);
  virtual size_t prepare_train_and_test_data(const float ratio = DIV_RATIO);
  virtual void train_and_test_classfier() = 0;
  virtual float predict(const cv::Mat &) = 0;
  virtual std::pair<bool, size_t> load() = 0;

public:
  ClassifierIf(std::string);
  ClassifierIf(const ClassifierIf &) = delete;
  ClassifierIf(const ClassifierIf &&) = delete;
  virtual ~ClassifierIf();
};