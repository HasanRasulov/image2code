#pragma once

#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <opencv2/objdetect.hpp>

#include "HashIf.hpp"
#include "Preprocess.hpp"
#include "Logger.hpp"
#include "CharSegmenter.hpp"

#include <algorithm>
#include <iterator>
#include <filesystem>

/**
 * @brief (WIDTH_HOG - BLOCK_SIZE) % BLOCKSTRIDE_SIZE == 0
 *
 */
class HOGHashOfImg : public HashIf
{
     static constexpr size_t BLOCK_SIZE{10u};
     static constexpr size_t CELL_SIZE{5u};
     static constexpr size_t BLOCKSTRIDE_SIZE{5u};
     static constexpr size_t BINS{9u};

public:
     int number_of_characters;
     std::vector<std::pair<char, std::vector<float>>> hash;

     HOGHashOfImg(std::filesystem::path);
     ~HOGHashOfImg() = default;
     void calculate_hash() override;
     std::vector<std::vector<float>> calculate_hash_sample();

private:
};