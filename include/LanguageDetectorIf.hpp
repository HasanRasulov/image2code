#ifndef __LANGUAGE__DETECTOR_HPP__
#define __LANGUAGE__DETECTOR_HPP__

#include "HashIf.hpp"

class LanguageDetectorIf
{

public:
  virtual Classes detectLanguage(std::string) = 0;
  virtual Classes detectLanguage(std::filesystem::path) = 0;
};

#endif