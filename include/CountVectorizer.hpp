#pragma once
#include <string>
#include <vector>
#include <map>
#include <iterator>
#include <fstream>

#include "Logger.hpp"
#include "StringPreProcess.hpp"

using namespace std::string_literals;

class CountVectorizer
{

  std::string text;
  std::vector<size_t> frequencies;

public:
  bool skip_word(std::string, size_t min_len = 0u);

  // keep these two public for testing
  std::vector<std::string> get_tokens();
  std::vector<std::string> get_ngrams(uint8_t grams = 2u);

  std::multimap<std::string, size_t> calculate_frequencies();

public:
  CountVectorizer(std::filesystem::path path_file);
  CountVectorizer(std::string str) : text{std::move(str)} {}
  ~CountVectorizer() = default;
  std::vector<size_t> get_frequencies() const;
};
