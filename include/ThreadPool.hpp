
#pragma once

#include <atomic>
#include <condition_variable>
#include <functional>
#include <future>
#include <queue>
#include <thread>

#include "HashIf.hpp"

class ThreadPool
{

public:
  using Task_type = void(std::shared_ptr<HashIf>);

private:
  size_t workers_num;

  std::atomic<bool> running;
  std::vector<std::thread> workers;
  std::queue<std::shared_ptr<std::packaged_task<void()>>> tasks;
  std::mutex idle_worker_lock;
  std::condition_variable idle_worker;

public:
  ThreadPool(size_t);
  ~ThreadPool();
  ThreadPool(const ThreadPool &) = delete;
  ThreadPool &operator=(const ThreadPool &) = delete;
  void submit(std::function<Task_type>, std::shared_ptr<HashIf>);
};
