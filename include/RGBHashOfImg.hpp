#pragma once

#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "HashIf.hpp"
#include "Preprocess.hpp"
#include "Logger.hpp"
#include <algorithm>
#include <iterator>
#include <filesystem>

class RGBHashOfImg : public HashIf
{
public:
     int number_of_characters;

     RGBHashOfImg(std::filesystem::path);
     ~RGBHashOfImg() = default;
     std::vector<std::pair<char, std::vector<float>>> hash;
     void calculate_hash() override;
     std::vector<std::vector<float>> calculate_hash_sample();

     friend std::ostream &operator<<(std::ostream &, const RGBHashOfImg &);
     static int getClass(const std::string &);
     void add_padding(size_t) override;
};