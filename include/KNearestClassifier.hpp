#pragma once
#include "ClassifierIf.hpp"

class KNearestClassifier : public ClassifierIf
{

  cv::Ptr<cv::ml::KNearest> model;

public:
  KNearestClassifier(std::string);
  ~KNearestClassifier();
  void train_and_test_classfier() override;

  float predict(const cv::Mat &) override;
  std::pair<bool, size_t> load() override;
};
