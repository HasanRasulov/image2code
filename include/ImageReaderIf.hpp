#pragma once

#include <filesystem>
#include <fstream>
#include <initializer_list>
#include <opencv2/opencv.hpp>
#include <utility>
#include <vector>

class ImageReaderIf
{
protected:
  std::vector<std::filesystem::path> path2imgs;
  std::filesystem::path path2result;
  std::string result;
  std::vector<cv::Mat> imgs;

public:
  virtual void image2code() = 0;

  ImageReaderIf(std::initializer_list<std::filesystem::path> paths, std::filesystem::path result);

  operator std::filesystem::path();

  operator std::string();
};
