#ifndef __OUTPUTCORRECTER_HPP__
#define __OUTPUTCORRECTER_HPP__

#include <string>
#include <utility>
#include <vector>
#include <filesystem>
#include <fstream>
#include <json/json.h>
#include <cstdint>
#include "Keyword.hpp"
#include "HashIf.hpp"
#include "Logger.hpp"

class OutputCorrecter
{

    static inline const std::string PATH_TO_FOLDER = "../data/";
    static inline constexpr float MIN_SIMILIARLITY = .85;
    std::string source;
    std::string keywords_filename;
    std::string cls_string;
    std::vector<Keyword> keywords;
    Classes lang_cls;

    void read_keywords();
    std::filesystem::path get_keywords_file();
    int getEditDistance(const std::string &, const std::string &) const;
    float get_similarity_level(const std::string &, const std::string &) const;

public:
    OutputCorrecter(std::string source) : source{std::move(source)}
    {
    }
    std::string correct(Classes);
    ~OutputCorrecter() = default;
};

#endif