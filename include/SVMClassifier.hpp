#pragma once
#include "ClassifierIf.hpp"

class SVMClassifier : public ClassifierIf
{

  cv::Ptr<cv::ml::SVM> model;

public:
  SVMClassifier(std::string);
  ~SVMClassifier();

  void train_and_test_classfier() override;

  float predict(const cv::Mat &) override;
  std::pair<bool, size_t> load() override;
};
