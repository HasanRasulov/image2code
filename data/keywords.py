{"py":[
    
    {"keyword" : "False","confidence_val": 10.11},
    {"keyword" : "await","confidence_val": 0.55},
    {"keyword" : "else","confidence_val": 0.55},
    {"keyword" : "import","confidence_val": 0.55},
    {"keyword" : "pass","confidence_val": 10.11},
    {"keyword" : "None","confidence_val": 0.55},
    {"keyword" : "break","confidence_val": 0.55},
    {"keyword" : "except","confidence_val": 0.55},
    {"keyword" : "in","confidence_val": 10.11},
    {"keyword" : "raise","confidence_val": 0.55},
    {"keyword" : "True","confidence_val": 0.55},
    {"keyword" : "class","confidence_val": 0.55},
    {"keyword" : "finally","confidence_val": 10.11},
    {"keyword" : "is","confidence_val": 0.55},
    {"keyword" : "return","confidence_val": 0.55},
    {"keyword" : "and","confidence_val": 0.55},
    {"keyword" : "continue","confidence_val": 10.11},
    {"keyword" : "for","confidence_val": 0.55},
    {"keyword" : "lambda","confidence_val": 0.55},
    {"keyword" : "try","confidence_val": 0.55},
    {"keyword" : "as","confidence_val": 10.11},
    {"keyword" : "def","confidence_val": 0.55},
    {"keyword" : "from","confidence_val": 0.55},
    {"keyword" : "nonlocal","confidence_val": 0.55},
    {"keyword" : "while","confidence_val": 10.11},
    {"keyword" : "assert","confidence_val": 0.55},
    {"keyword" : "del","confidence_val": 0.55},
    {"keyword" : "global","confidence_val": 0.55},
    {"keyword" : "not","confidence_val": 10.11},
    {"keyword" : "with","confidence_val": 0.55},
    {"keyword" : "async","confidence_val": 0.55},
    {"keyword" : "elif","confidence_val": 0.55},
    {"keyword" : "if","confidence_val": 10.11},
    {"keyword" : "or","confidence_val": 0.55},
    {"keyword" : "yield","confidence_val": 0.55}   

]}
