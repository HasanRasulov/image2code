from numpy import number
import requests
import validators
import os
import shutil
import stat




def handleRemoveReadonly(func, path, exc):
 if not os.access(path, os.W_OK):
        os.chmod(path, stat.S_IWUSR)
        func(path)
 else:
        raise

import sys

if len(sys.argv) != 2:
    print('Default is all.[number of files per class]')
    number_of_files_per_cls = sys.maxsize
else: 
    number_of_files_per_cls = int(sys.argv[1])

langdata = '..\langdata'
if os.path.isdir(langdata):
    shutil.rmtree(langdata)

os.mkdir(langdata)

max_filename_cp = 0 
classes = ['cpp', 'java','py'] #TODO:support for non-standart extensiosn .*cc
for cls in classes:
    
    links_file = open('links_to_raw_'+cls+'_data.txt', 'r')

    max_filename = 0
    path = langdata + "\\" +cls + '\\'

    # remove folders first
    if os.path.isdir(path):
        os.rmdir(path)

    os.mkdir(path)
    current_files = os.listdir(path)

    for i in range(len(current_files)):
        filename = int(os.path.splitext(current_files[i])[0])
        if filename > max_filename:
            max_filename = filename

    links = links_file.readlines()

    for link in links:
        if not validators.url(link):
            continue
        r = os.system("git clone " + link)
        repo_name = link[link.rfind('/') + 1 : link.rfind('.')]
        if r == 0:
            for root,dirs,files in os.walk(repo_name):
                for file in files:
                    if file.endswith("." + cls):
                       if max_filename < number_of_files_per_cls: 
                          max_filename += 1
                          os.rename(os.path.join(root, file), path+str(max_filename)+'.'+cls)

        shutil.rmtree(repo_name,ignore_errors=False, onerror=handleRemoveReadonly)



print('-------------------------------------------------------------------------------------------------------------------')

for cls in classes:
    path = langdata + "\\" +cls + '\\'
    print(str(len([src for src in os.listdir(path) if not os.path.isfile(src)])) + " files downloaded for " + cls) 

print('-------------------------------------------------------------------------------------------------------------------')    