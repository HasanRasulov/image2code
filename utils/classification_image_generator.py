import cv2
import numpy as np
import string
import os
import shutil
import sys

all_chars = string.printable[0:len(string.printable)-4]
#all_chars = ['#','i','n','c','l','u','d','e','"','i','o','s','>','','i','n','t',' ','m','a','i','n','(',')','\n','{','r','e','t','u','r','n',' ','0',';','\n','}']

print(len(all_chars))
print(all_chars)
dir_name = "../imgdata/"

if os.path.isdir(dir_name):
    shutil.rmtree(dir_name)

os.mkdir(dir_name)

fonts = [cv2.FONT_HERSHEY_SIMPLEX, cv2.FONT_HERSHEY_DUPLEX,cv2.FONT_HERSHEY_PLAIN,cv2.FONT_HERSHEY_TRIPLEX,cv2.FONT_HERSHEY_COMPLEX,cv2.FONT_HERSHEY_COMPLEX_SMALL]#,cv2.FONT_HERSHEY_SCRIPT_SIMPLEX,cv2.FONT_HERSHEY_SCRIPT_COMPLEX,cv2.FONT_ITALIC ]
#fonts = [cv2.FONT_ITALIC]
image = np.zeros((2400, 2400, 3), np.uint8)
fontScale = [1,2,3]
distance = [50,40,30]
i = 0
number_of_imgs = 0
for scale in fontScale:
    for font in fonts:
        image.fill(255)
        origin_x = 100
        origin_y = 100
        max_height = 0
        max_width = 0
        for ch in all_chars:
            cv2.putText(image, ch, (origin_x, origin_y), font,
                        scale, (0, 0, 0), 1, cv2.LINE_AA)
            size, baseline = cv2.getTextSize(ch, font, scale, 1)
            max_height = max(max_height,size[1])
            max_width = max(max_width,size[0])
                
            print(ch +" ---font:" +str(font)+ "scale:" + str(scale) + "size:" + str(size))
            origin_x = origin_x + size[0] + distance[i] * scale
            if origin_x >= image.shape[0]-distance[i] * scale:
                origin_y = origin_y + size[1] + distance[i] * scale
                origin_x = 100
        
        cv2.imwrite(dir_name+"Result_"+str(max_width)+"_"+str(max_height)+".png", image)
        number_of_imgs+=1
    i+=1    

print('Number of images generated:'+str(number_of_imgs))