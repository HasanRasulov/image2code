import cv2
import numpy as np
import os
import sys
import shutil

if len(sys.argv) != 2:
    print('Wrong number of parameters.')

filename = str(sys.argv[1])

if not os.path.exists(filename):
    print('Error: File does not exist')

dir_name = "../test/data/"

if os.path.isdir(dir_name):
    shutil.rmtree(dir_name)

os.mkdir(dir_name)

file = open(filename, mode='r')
txt = file.read()
file.close()

# ,cv2.FONT_HERSHEY_TRIPLEX,cv2.FONT_HERSHEY_COMPLEX]
#fonts = [cv2.FONT_HERSHEY_SIMPLEX,cv2.FONT_HERSHEY_DUPLEX, cv2.FONT_HERSHEY_PLAIN]
fonts = [cv2.FONT_HERSHEY_SIMPLEX, cv2.FONT_HERSHEY_DUPLEX,cv2.FONT_HERSHEY_PLAIN,cv2.FONT_HERSHEY_TRIPLEX,cv2.FONT_HERSHEY_COMPLEX,cv2.FONT_HERSHEY_COMPLEX_SMALL,cv2.FONT_HERSHEY_SCRIPT_SIMPLEX,cv2.FONT_HERSHEY_SCRIPT_COMPLEX,cv2.FONT_ITALIC ]
chars = list(txt)
print(chars)
print(len(chars))
fontScale = [1, 2, 3]
image = np.zeros((4000, 4000, 3), np.uint8)
y0, dy = 40, 5
x0, dx = 10, 30
x, y = x0, y0       
distance = [7,9,13]
j = 0

for scale in fontScale:
    for font in fonts:
        image.fill(255)
        image.fill(255)
        origin_x = 100
        origin_y = 100
        max_height = 0
        max_width = 0
        for ch in chars:
            if ch!= '\n' and ch!='\t':
                #print(ch +" ---font:" +str(font)+ "scale:" + str(scale))
                cv2.putText(image, ch, (origin_x, origin_y), font,
                        scale, (0, 0, 0), 1, cv2.LINE_AA)
            size, baseline = cv2.getTextSize(ch, font, scale, 1)
            max_height = max(max_height,size[1])
            max_width = max(max_width,size[0])
                
            origin_x = origin_x + size[0]
            if origin_x >= image.shape[0] or ch == '\n':
                origin_y = origin_y + size[1] + distance[j] * scale
                origin_x = 100
        
        cv2.imwrite(dir_name+"Test_"+str(max_width)+"_"+str(max_height)+".png", image)
    j+=1    
