import wget
import os

traindata_eng = "https://github.com/tesseract-ocr/tessdata/raw/main/eng.traineddata"

filename = wget.download(traindata_eng)
dst_path = "../build/"+filename

os.rename(filename,"../build/"+filename)

print(dst_path)