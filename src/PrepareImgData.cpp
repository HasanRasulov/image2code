#include "PrepareImgData.hpp"

PrepareImgData::PrepareImgData(std::filesystem::path path_to_data,
                               std::filesystem::path save_to)
    : PrepareDataIf(path_to_data, save_to) {}

uint16_t PrepareImgData::save_to_csv(bool padding) const //TODO::Vocabulary size is greater than 2 byte 
{

  const std::source_location location{std::source_location::current()};

  if (std::filesystem::exists(save_to))
  {
    log<LogLevel::INFO>(location, "Data is prepared: %s\n", save_to.string().c_str());
    return 0; /// correct
  }

  auto dir_it = std::filesystem::recursive_directory_iterator{path};

  uint32_t number_of_files =
      std::count_if(begin(dir_it), end(dir_it), [](const auto &dir_entry)
                    { return dir_entry.is_regular_file(); });

  if(!number_of_files){
    log<LogLevel::INFO>(location, "No training pictures found: %s\n", path.string().c_str());
    return 0;
  }                  

  log(location, " Number of files:%u\n", number_of_files);

  uint32_t number_of_threads =
      number_of_files > std::thread::hardware_concurrency()
          ? std::thread::hardware_concurrency()
          : number_of_files;

  log(location, " Number of threads: %u\n", number_of_threads);

  std::vector<std::shared_ptr<HOGHashOfImg>> results;

  {

    ThreadPool pool(number_of_threads);

    // iterate over all the files and call feature extractor 
    for (auto &dir_entry :
         std::filesystem::recursive_directory_iterator{path})
    {

      if (dir_entry.is_regular_file())
      {

        auto result = std::make_shared<HOGHashOfImg>(dir_entry.path());

        pool.submit(&HashIf::calculate_hash, result);

        results.emplace_back(result);
      }
    }
  }
  
  // added for only RGBHashOfImg
  size_t max_size{};

  for (const auto& rgbhash : results)
  {
    for (const std::pair<char, std::vector<float>> &data_entry :
         rgbhash->hash)
    {
      if (max_size < data_entry.second.size())
        max_size = data_entry.second.size();
    }
  }

  if (padding)
  {

    std::for_each(results.begin(), results.end(), [max_size](auto &rgbhash)
                  { rgbhash->add_padding(max_size); });
  }

  std::ofstream imgdata(save_to);

  if (!imgdata.is_open())
  {
    log<LogLevel::ERROR>(location, "The stream to %s could not be opened\n", save_to.string().c_str());
    return -1;
  }

  auto number_of_entries = 0u;
  constexpr uint8_t number_of_duplications = 3u;
  auto number_of_failed_images = 0;

  for (const auto &entry : results)
  {
    if (entry->hash.empty()){
      number_of_failed_images++;
      continue;
    }  
    for (const std::pair<char, std::vector<float>> &hash : entry->hash)
    {
      for (auto i = 0u; i < number_of_duplications; i++)
      {
        imgdata << hash.first << ",";

        std::copy(hash.second.begin(), hash.second.end(),
                  infix_ostream_iterator<float>(imgdata, ","));
        imgdata << std::endl;
        number_of_entries++;
      }
    }
  }
  
  imgdata.close();
  if(number_of_failed_images > static_cast<size_t>(number_of_files/2)){
    log<LogLevel::ERROR>(location,"Data could not be prepared/n%u %% files failed",static_cast<unsigned>(static_cast<float>(number_of_failed_images)/number_of_files*100));
    std::filesystem::remove(save_to);
    return 0;
  }
  log(location, "Number of failed images:%u\nNumber of character entries: %u\n", number_of_failed_images, number_of_entries);

  return max_size;
}
