#include "ImageReaderTesseract.hpp"

/**
 * @brief convert the images into text
 *
 */
void ImageReaderTesseract::image2code()
{

  tesseract::TessBaseAPI *ocr = new tesseract::TessBaseAPI();
  ocr->Init(".", "eng", tesseract::OEM_LSTM_ONLY);
  ocr->SetPageSegMode(tesseract::PSM_AUTO);

  for (const auto &path : path2imgs)
  {
    Pix *img = pixRead(path.string().c_str());
    ocr->SetImage(img);
    char *outText = ocr->GetUTF8Text();
    result += outText;
    delete[] outText;

    pixDestroy(&img);
  }

  ocr->End();
  delete ocr;
}
