#include "KNearestClassifier.hpp"

void KNearestClassifier::train_and_test_classfier()
{
    const std::source_location location{std::source_location::current()};

    log(location, "Training the classifier ...\n");
    model = cv::ml::KNearest::create();
    model->setDefaultK(2);
    model->setIsClassifier(true);
    model->train(train_data);

    cv::Mat resp;
    auto err = model->calcError(train_data, true, resp);
    log(location, "Error:%f\n", err);

    if (!filename_to_save.empty())
    {
        model->save(filename_to_save);
    }
}

float KNearestClassifier::predict(const cv::Mat &sample)
{
    cv::Mat result(0, 0, CV_32FC1);

    model->findNearest(sample, 2, result);

    auto res = result.at<float>(0, 0);
    return res;
}
KNearestClassifier::KNearestClassifier(std::string filename_to_read) : ClassifierIf{filename_to_read}
{
    const std::source_location location{std::source_location::current()};

    filename_to_save = get_classifier_name(location);
    filename_to_save += "_model.data";
}

std::pair<bool, size_t> KNearestClassifier::load()
{
    std::pair<bool, size_t> number_of_variables{true, 0u};
    std::ifstream mod(filename_to_save);

    std::string line;
    while (std::getline(mod, line))
    {

        if (line.find("cols") != std::string::npos)
        {
            auto it = std::find_if(line.begin(), line.end(), [](char ch)
                                   { return isdigit(ch); });
            auto pos = std::distance(line.begin(), it);
            number_of_variables.second = atoi(line.substr(pos, line.length() - pos).c_str());
            break;
        }
    }

    mod.close();

    model = cv::ml::KNearest::load(filename_to_save);

    return number_of_variables;
}
KNearestClassifier::~KNearestClassifier() {}