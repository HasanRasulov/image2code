#include "Keyword.hpp"

std::ostream &operator<<(std::ostream &os, const Keyword &obj)
{
    os << "Keyword:" << obj.actual_keyword << "\n";
    os << "Confidence Value:" << obj.confidence_value << std::endl;

    return os;
}

std::string Keyword::getKeyword() const
{
    return actual_keyword;
}

float Keyword::getConfidence() const
{
    return confidence_value;
}

void Keyword::setKeyword(std::string kyw)
{
    actual_keyword = kyw;
}

void Keyword::setConfidence(float val)
{
    confidence_value = val;
}
