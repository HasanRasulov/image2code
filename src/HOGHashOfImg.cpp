#include "HOGHashOfImg.hpp"

/**
 * @brief Construct a new HOGHashOfImg::HOGHashOfImg object
 *
 * @param path
 */
HOGHashOfImg::HOGHashOfImg(std::filesystem::path path) : HashIf{path}
{

    const std::source_location location{std::source_location::current()};

    auto pos_first_underscore = path.string().find_first_of("_");
    auto pos_underscore = path.string().find_last_of("_");
    auto pos_dot = path.string().find_first_of(".");
    // the sample will used for prediction
    if (pos_first_underscore == std::string::npos || pos_underscore == std::string::npos)
    {
        max_width = max_height = 0;
        return;
    }
    // sample belong to training set
    max_width = atoi(path.string().substr(pos_first_underscore + 1, pos_underscore - pos_first_underscore).c_str());
    max_height = atoi(path.string().substr(pos_underscore + 1, pos_dot - pos_underscore).c_str());
    max_height += static_cast<size_t>(max_height * 0.2f);
    log(location, "Max height and width of character.Image: %s   MAX_Height:%u MAX_Width:%u\n", path.string().c_str(), max_height, max_width);
}

/**
 * @brief to calculate the feature vector for training images
 *
 */
void HOGHashOfImg::calculate_hash()
{

    const std::source_location location{std::source_location::current()};

    if (!std::filesystem::exists(get_path()))
    {
        log<LogLevel::ERROR>(location, "File does not exists:%s", get_path().string().c_str());
        return;
    }

    cv::Mat img = cv::imread(get_path().string());
    if (DRAWROI)
        cv::imshow("Preprocess", img);

    cv::Mat edge_img = Preprocess::preprocess(img);

    std::vector<std::vector<cv::Point>> contours;
    cv::findContours(edge_img, contours, cv::RETR_EXTERNAL,
                     cv::CHAIN_APPROX_NONE);

    std::vector<cv::Rect> rois = getRoisForCharacters(contours, std::bind(&HashIf::is_near_rects, this, std::placeholders::_1, std::placeholders::_2));

    log(location, "%s : Reduced number of rois : %u\tNumber of contours : %u\n", get_path().string().c_str(), rois.size(), contours.size());

    sort_contours(rois);

    this->number_of_characters = rois.size();

    if (DRAWROI)
        draw_rois(img, rois);

    if (number_of_characters != HashIf::classes.size())
    {
        log<LogLevel::ERROR>(location, "[Error:%s] Wrong number of rois.Expect:%u Actual: %u\n", get_path().string().c_str(), HashIf::classes.size(), rois.size());
        return;
    }

    for (size_t i = 0; i < number_of_characters; i++)
    {

        cv::HOGDescriptor hog(
            cv::Size(Preprocess::WIDTH_HOG, Preprocess::HEIGHT_HOG), // winSize
            cv::Size(BLOCK_SIZE, BLOCK_SIZE),                        // blocksize
            cv::Size(BLOCKSTRIDE_SIZE, BLOCKSTRIDE_SIZE),            // blockStride,
            cv::Size(CELL_SIZE, CELL_SIZE),                          // cellSize,
            BINS,                                                    // nbins,
            1,                                                       // derivAper,
            -1,                                                      // winSigma,
            cv::HOGDescriptor::L2Hys,                                // histogramNormType,
            0.2,                                                     // L2HysThresh,
            1,                                                       // gammal correction,
            64,                                                      // nlevels=64
            1                                                        // Use signed gradients
        );                                                           // Use signed gradients

        std::vector<float> descriptors;
        cv::Mat char_img = Preprocess::resize(img(rois[i]));
        hog.compute(char_img, descriptors);

        hash.push_back(std::make_pair(HashIf::classes[i], descriptors));
        descriptors.clear();
    }
}
/**
 * @brief to calculate the feature vector for sample images
 *
 * @return std::vector<std::vector<float>>
 */
std::vector<std::vector<float>> HOGHashOfImg::calculate_hash_sample()
{

    std::vector<std::vector<float>> hash;

    const std::source_location location{std::source_location::current()};

    if (!std::filesystem::exists(get_path()))
    {
        log<LogLevel::ERROR>(location, "File does not exists:%s", get_path().string().c_str());
        return hash;
    }

    cv::Mat img = cv::imread(get_path().string());

    auto chars = getRoisForCharacters(img);
    this->number_of_characters = chars.size();

    for (size_t i = 0; i < this->number_of_characters; i++)
    {

        cv::HOGDescriptor hog(
            cv::Size(Preprocess::WIDTH_HOG, Preprocess::HEIGHT_HOG), // winSize
            cv::Size(BLOCK_SIZE, BLOCK_SIZE),                        // blocksize
            cv::Size(BLOCKSTRIDE_SIZE, BLOCKSTRIDE_SIZE),            // blockStride,
            cv::Size(CELL_SIZE, CELL_SIZE),                          // cellSize,
            BINS,                                                    // nbins,
            1,                                                       // derivAper,
            -1,                                                      // winSigma,
            cv::HOGDescriptor::L2Hys,                                // histogramNormType,
            0.2,                                                     // L2HysThresh,
            1,                                                       // gammal correction,
            64,                                                      // nlevels=64
            1                                                        // Use signed gradients
        );

        std::vector<float> descriptors;

        cv::Mat char_img = Preprocess::resize(chars[i]);
        cv::imwrite(std::to_string(i) + "_char_sample.png", char_img);

        hog.compute(char_img, descriptors);

        hash.push_back(descriptors);
        descriptors.clear();
    }

    return hash;
}
