#include "OutputCorrecter.hpp"

/**
 * @brief function will return keywords file name
 *
 * @return std::filesystem::path
 */
std::filesystem::path OutputCorrecter::get_keywords_file()
{

  const std::source_location location{std::source_location::current()};

  cls_string = HashIf::getClass(lang_cls);

  keywords_filename = PATH_TO_FOLDER + "keywords." + cls_string;

  std::filesystem::path keywords_path{keywords_filename};

  if (!std::filesystem::exists(keywords_path))
  {
    log<LogLevel::ERROR>(location, "Keywords file could not be found:%s", keywords_filename.c_str());
  }

  return keywords_path;
}
/**
 * @brief  function will read the keyword json file.
 *
 */
void OutputCorrecter::read_keywords()
{

  const std::source_location loc{std::source_location::current()};

  auto keyword_path = get_keywords_file();
  std::ifstream keys_stream{keyword_path.string()};

  std::string strjson;
  if (!keys_stream.is_open())
  {
    log(location, "%s keyword file could not be opened!!!\n", keyword_path.string().c_str());
    return;
  }

  std::stringstream str;

  std::string strline;
  while (std::getline(keys_stream, strline))
  {
    str << strline;
  }

  keys_stream.close();

  Json::CharReaderBuilder reader; //  Reader
  Json::Value jsonData;           //  The value of value can be any object
  std::string errs;

  if (Json::parseFromStream(reader, str, &jsonData, &errs))

  {
    auto key_arr = jsonData[cls_string];
    log(loc, "Number of keywords %d in %s\n", key_arr.size(), keyword_path.string().c_str());

    int i = 0;
    while (i < key_arr.size())
    {
      keywords.push_back({key_arr[i]["keyword"].asString(), key_arr[i]["confidence_val"].asFloat()});
      i++;
    }
  }
  else
  {
    log<LogLevel::ERROR>(loc, "Could not parse keyword data as JSON:%s\n", str.str().c_str());
    return;
  }

  // std::for_each(keywords.begin(),keywords.end(),[&loc](const auto& k){log(loc,"%s\n",k.getKeyword().c_str());});
}

/**
 * @brief get edit distance
 * @refitem https://www.techiedelight.com/levenshtein-distance-edit-distance-problem/
 */
int OutputCorrecter::getEditDistance(const std::string &first, const std::string &second) const
{
  int m = first.length();
  int n = second.length();

  // For all pairs of `i` and `j`, `T[i, j]` will hold the Levenshtein distance
  // between the first `i` characters of `X` and the first `j` characters of `Y`.
  // Note that `T` holds `(m+1)×(n+1)` values.
  int T[m + 1][n + 1];

  // initialize `T` by all 0's
  memset(T, 0, sizeof T);

  // we can transform source prefixes into an empty string by
  // dropping all characters

  for (int i = 1; i <= m; i++)
  {
    T[i][0] = i; // (case 1)
  }

  // we can reach target prefixes from empty source prefix
  // by inserting every character

  for (int j = 1; j <= n; j++)
  {
    T[0][j] = j; // (case 1)
  }

  int substitutionCost;

  // fill the lookup table in a bottom-up manner
  for (int i = 1; i <= m; i++)
  {
    for (int j = 1; j <= n; j++)
    {
      if (first[i - 1] == second[j - 1])
      {                       // (case 2)
        substitutionCost = 0; // (case 2)
      }
      else
      {
        substitutionCost = 1; // (case 3c)
      }
      T[i][j] = std::min(std::min(T[i - 1][j] + 1,            // deletion (case 3b)
                                  T[i][j - 1] + 1),           // insertion (case 3a)
                         T[i - 1][j - 1] + substitutionCost); // replace (case 2 & 3c)
    }
  }

  return T[m][n];
}

/**
 * @brief correct function will try to correct the acquired token based on given language
 *
 * @param cls given language
 * @return std::string
 */
std::string OutputCorrecter::correct(Classes cls)
{
  const std::source_location location{std::source_location::current()};

  lang_cls = cls;
  read_keywords();

  if (keywords.empty())
  {
    log<LogLevel::ERROR>(location, "keywords is empty!!\n");
    return source;
  }

  std::string output = source;
  for (const auto &kyw : keywords)
  {
    for (size_t i = 0; i < source.size() - kyw.getKeyword().size();)
    {
      std::string possible_match{source, i, kyw.getKeyword().size()};
      auto similarity = get_similarity_level(kyw.getKeyword(), possible_match);

      if (similarity > MIN_SIMILIARLITY)
      {
        log(location, "%s --- %s = %f\n", kyw.getKeyword().c_str(), possible_match.c_str(), similarity);
        output.replace(i, kyw.getKeyword().size(), kyw.getKeyword());
        i += kyw.getKeyword().size();
      }
      else
      {
        i++;
      }
    }
  }
  return output;
}
/**
 * @brief function will return float value which signify the similarity between two given string
 *
 * @param keyword actual keyword
 * @param found_keyword predicted keyword
 * @return float
 */
float OutputCorrecter::get_similarity_level(const std::string &keyword, const std::string &found_keyword) const
{
  const std::source_location location{std::source_location::current()};

  double max_length = std::max(keyword.length(), found_keyword.length());

  if (max_length > 0)
  {
    auto dist = getEditDistance(keyword, found_keyword);
    return (max_length - dist) / max_length;
  }
  return .0f;
}
