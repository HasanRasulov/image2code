#include "HashIf.hpp"
/**
 * @brief add padding no side affect
 *
 * @param size
 */
void HashIf::add_padding(size_t size)
{

  const std::source_location location{std::source_location::current()};
  log(location, "Default Implementation.No padding will be added\n");
}
/**
 * @brief get float value for class
 *
 * @param class_name
 * @return int
 */
int HashIf::getClass(const std::string &class_name)
{

  for (const auto &it : lang_classes)
  {
    if (it.second == class_name)
      return static_cast<int>(it.first);
  }

  return 0;
}
/**
 * @brief get string version of class
 *
 * @param cls
 * @return std::string
 */
std::string HashIf::getClass(Classes cls)
{
  std::map<Classes, std::string>::const_iterator it;

  if ((it = lang_classes.find(cls)) == lang_classes.end())
  {
    return "Unknown";
  }

  return it->second;
}
/**
 * @brief draw given rectangles on image
 *
 * @param img
 * @param rois
 */
void HashIf::draw_rois(cv::Mat &img, const std::vector<cv::Rect> &rois)
{
  int key{};
  const std::source_location location{std::source_location::current()};

  for (size_t i = 0; i < rois.size(); i++)
  {
    cv::rectangle(img, rois[i], cv::Scalar(255, 0, 0));
    cv::namedWindow(get_path().filename().string(), cv::WINDOW_AUTOSIZE);
    cv::imshow(get_path().filename().string(), img);
    log(location, "Current character:%c\n", i < classes.size() ? classes[i] : '?');

    key = cv::waitKey(0);
    if (key == 27)
    {
      exit(0);
    }
  }
}
/**
 * @brief is two rectangles enough close to each-other
 *
 * @param r1
 * @param r2
 * @return true
 * @return false
 */
bool HashIf::is_near_rects(const cv::Rect &r1, const cv::Rect &r2)
{
  return std::abs(r1.x - r2.x) <= max_width && std::abs(r1.y - r2.y) <= max_height;
}
/**
 * @brief is r2 inside r1
 *
 * @param r1
 * @param r2
 * @return true
 * @return false
 */
bool HashIf::is_inside(const cv::Rect &r1, const cv::Rect &r2)
{
  return (r2.x + r2.width <= r1.x + r1.width && r2.x >= r1.x && r2.y >= r1.y && r2.y + r2.height <= r1.y + r1.height) || (r1.x + r1.width <= r2.x + r2.width && r1.x >= r2.x && r1.y >= r2.y && r1.y + r1.height <= r2.y + r2.height);
}
/**
 * @brief sort contours horizantally and vertically
 *
 * @param contour_rects
 */
void HashIf::sort_contours(std::vector<cv::Rect> &contour_rects)
{

  auto is_more_right = [](auto r1, auto r2)
  { return r1.x < r2.x; };

  auto is_more_up = [](auto r1, auto r2)
  { return r1.y < r2.y; };

  auto is_different_row = [this](auto r1, auto r2)
  {
    return std::abs(r1.y - r2.y) > max_height;
  };

  std::sort(contour_rects.begin(), contour_rects.end(), is_more_up);

  auto beg = contour_rects.begin();
  std::vector<cv::Rect>::iterator end;

  do
  {
    end = std::adjacent_find(beg, contour_rects.end(), is_different_row);
    std::sort(beg, (end == contour_rects.end() ? end : end + 1), is_more_right);
    beg = (end == contour_rects.end() ? end : end + 1);

  } while (end != contour_rects.end());
}
/**
 * @brief find the bounding box of all the regions in rects
 *
 * @param rects
 * @return cv::Rect
 */
cv::Rect HashIf::get_bounding_rect(const std::vector<cv::Rect> &rects)
{

  auto min_x = std::min_element(rects.begin(), rects.end(), [](auto r1, auto r2)
                                { return r1.x < r2.x; });
  auto min_x_val = min_x->x;

  auto min_y = std::min_element(rects.begin(), rects.end(), [](auto r1, auto r2)
                                { return r1.y < r2.y; });
  auto min_y_val = min_y->y;

  auto max_x = std::max_element(rects.begin(), rects.end(), [](auto r1, auto r2)
                                { return r1.x + r1.width < r2.x + r2.width; });
  auto max_x_val = max_x->x + max_x->width;

  auto max_y = std::max_element(rects.begin(), rects.end(), [](auto r1, auto r2)
                                { return r1.y + r1.height < r2.y + r2.height; });
  auto max_y_val = max_y->y + max_y->height;

  return cv::Rect(cv::Point(min_x_val, min_y_val),
                  cv::Size(max_x_val - min_x_val, max_y_val - min_y_val));
}

/**
 * @brief get region of interests fro given contours
 *
 * @param contours
 * @param should_be_joined should we join two rectangles
 * @return std::vector<cv::Rect>
 */
std::vector<cv::Rect> HashIf::getRoisForCharacters(std::vector<std::vector<cv::Point>> &contours, std::function<bool(const cv::Rect &, const cv::Rect &)> should_be_joined)
{
  const std::source_location location{std::source_location::current()};

  std::vector<std::pair<cv::Rect, bool>> rois;

  std::transform(contours.begin(), contours.end(), std::back_inserter(rois), [](auto contour)
                 { return std::make_pair(cv::boundingRect(contour), false); });
  log(location, "Number of rois before reduction : %u\n", rois.size());

  std::vector<cv::Rect> reduced_rois;

  for (auto it = rois.begin(); it != rois.end(); it++)
  {
    if (it->second)
      continue;

    std::vector<cv::Rect> near_rects;
    auto iter = it;

    while ((iter = std::find_if(iter, rois.end(), [this, &it, &should_be_joined](auto roi2)
                                { return /*!roi2.second && */ should_be_joined(it->first, roi2.first); })) != rois.end()) // is_near_rects
    {
      near_rects.push_back(iter->first);
      iter->second = true;
      iter++;
    }

    if (near_rects.empty())
      near_rects.push_back(it->first);

    cv::Rect character_roi = get_bounding_rect(near_rects);
    near_rects.clear();
    reduced_rois.push_back(character_roi);
  }
  std::sort(reduced_rois.begin(), reduced_rois.end(), [](const auto &r1, const auto &r2)
            { return r1.x < r2.x; });
  auto last = std::unique(reduced_rois.begin(), reduced_rois.end(), [](const auto &r1, const auto &r2)
                          { return (r1.x == r2.x && r1.y == r2.y && r1.width == r2.width && r1.height == r2.height); });
  reduced_rois.erase(last, reduced_rois.end());

  return reduced_rois;
}
/**
 * @brief get region of interests for given image
 *
 * @param img
 * @return std::vector<cv::Mat>
 */
std::vector<cv::Mat> HashIf::getRoisForCharacters(cv::Mat img)
{

  const std::source_location location{std::source_location::current()};

  CharSegmenter seg(img);

  auto chars = seg.getChars();

  log(location, "Number of characters:%u\n", chars.size());

  return chars;
}