#include "ThreadPool.hpp"
/**
 * @brief Construct a new Thread Pool
 *
 * @param num workers waiting
 */
ThreadPool::ThreadPool(size_t num) : workers_num{num}
{

  running = false;

  for (size_t i = 0; i < workers_num; i++)
  {

    workers.emplace_back([this]
                         {
      while (true) {

        std::packaged_task<void()> task;
        {
          std::unique_lock<std::mutex> lock{idle_worker_lock};

          idle_worker.wait(lock, [this] { return running || !tasks.empty(); });

          if (running && tasks.empty())
            break;

          task = std::move(*tasks.front());
          tasks.pop();
        } // to release the lock

        task();
      } });
  }
}
/**
 * @brief Destroy the Thread Pool:: Thread Pool object
 * join all the threads
 */
ThreadPool::~ThreadPool()
{

  running = true;

  idle_worker.notify_all();

  for (auto &worker : workers)
    if (worker.joinable())
      worker.join();
}
/**
 * @brief submit task to be executed by arbitrary thread
 *
 * @param task void(std::shared_ptr<HashIf>);
 * @param obj
 */
void ThreadPool::submit(std::function<Task_type> task,
                        std::shared_ptr<HashIf> obj)
{

  auto task_ptr =
      std::make_shared<std::packaged_task<void()>>(std::bind(task, obj));

  std::unique_lock<std::mutex> lock{idle_worker_lock};

  tasks.push(task_ptr);

  idle_worker.notify_one();
}
