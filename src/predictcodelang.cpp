#include <fstream>
#include <iostream>
#include <sstream>

#include "Logger.hpp"
#include "LanguageDetectorBagOfWords.hpp"
#include "HashIf.hpp"

int main(int argc, char *argv[])
{
  std::filesystem::path sample_name = "../test/data/test.txt";

  const std::source_location location{std::source_location::current()};

  if (argc < 2)
  {
    log<LogLevel::INFO>(location, "Sample file is not given\nDefault is %s\n", sample_name.string().c_str());
  }
  else
  {
    sample_name = argv[1];
    log<LogLevel::INFO>(location, "Sample file is %s\n", sample_name.string().c_str());
  }
  LanguageDetectorBagOfWords detector;
  Classes cls = detector.detectLanguage(sample_name);
  log(location, "Result:%s\n", HashIf::getClass(cls).c_str());

  return 0;
}
