#include "LanguageDetectorBagOfWords.hpp"

Classes LanguageDetectorBagOfWords::detectLanguage(std::string sample)
{
    const std::source_location location{std::source_location::current()};

    log<LogLevel::WARNNING>(location, "Not implemented\n");

    return Classes::UNKNOWN;
}

Classes LanguageDetectorBagOfWords::detectLanguage(std::filesystem::path path_to_sample)
{
    const std::source_location location{std::source_location::current()};

    ClassifierIf *cls = new CumulativeFrequencyAdditionClassifier("langdata.csv");

    std::pair<bool, size_t> number_of_variables{false, 0};

    if (!std::filesystem::exists(std::filesystem::path(cls->filename_to_save)))
    {
        log<LogLevel::INFO>(location, "Model has not been saved before: %s\n", cls->filename_to_save.c_str());

        number_of_variables = cls->read_data(static_cast<HashIf::converter>(HashIf::getClass));
        if (!number_of_variables.first && number_of_variables.second == 0)
        {
            log(location, "Could not read the data!!\n");
            return Classes::UNKNOWN;
        }

        cls->prepare_train_and_test_data();

        cls->train_and_test_classfier();
    }
    else
    {
        log(location, "Loading model:%s\n", cls->filename_to_save.c_str());

        number_of_variables = cls->load();
    }
    log(location, "Number of variables:%u\n", number_of_variables.second);
    
    if (path_to_sample.empty() || !std::filesystem::exists(path_to_sample))
    {
        log<LogLevel::ERROR>(location, "The path %s is empty or does not exist\n", path_to_sample.string().c_str());
        return Classes::UNKNOWN;
    }

    // special to CumulativeFrequencyAdditionClassifier
    auto freqs_sum = dynamic_cast<CumulativeFrequencyAdditionClassifier *>(cls)->get_local_frequencies_accumulation();
    auto sample_hash = ASCIIHashOfString::calculate_hash_sample(path_to_sample.string(), "langdata.csv", freqs_sum);
    log(location, "The vocabulary size:%u\n", sample_hash.size());
    if (std::all_of(sample_hash.begin(), sample_hash.end(), [](auto h)
                    { return h <= 0; }))
    {
        log<LogLevel::ERROR>(location, "No word was found from %s in vocabulary\n", path_to_sample.string().c_str());
        return Classes::UNKNOWN;
    }

    cv::Mat sample(1, sample_hash.size(), CV_64FC1, sample_hash.data());

    auto res = cls->predict(sample);
    delete cls;
    return static_cast<Classes>(res);
}
