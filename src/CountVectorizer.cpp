#include "CountVectorizer.hpp"

/**
 * @brief Construct a new Count Vectorizer object
 * @warning This constructor is for testing purpose supports only cpp class
 * @param path_file
 */
CountVectorizer::CountVectorizer(std::filesystem::path path_file)
{

  const std::source_location location{std::source_location::current()};
  std::cout << path_file << "\n";
  if (path_file.empty())
  {
    log<LogLevel::ERROR>(location, "The path %s is empty\n", path_file.c_str());
    return;
  }

  std::ifstream file(path_file.string());

  if (!file.is_open())
  {
    log<LogLevel::ERROR>(location, "The stream to %s could not be opened\n", path_file.string().c_str());
    return;
  }

  std::string whole_file{std::istreambuf_iterator<char>(file),
                         std::istreambuf_iterator<char>()};

  file.close();

  StringPreProcess processor(whole_file);

  processor.setSingleLineCommentChar("//");
  processor.setBlockCommentChars(std::make_pair("/*", "*/"));
  processor.setStringChars(std::vector<std::string>{"\""s, "\'"s});

  processor.remove_strings().remove_comments().remove_literals().remove_blank_lines();

  text = processor.getContent();
}
/**
 * @brief used to skip tokens
 *
 * @param beg
 * @param end
 * @param min_len
 * @return true if the size of string is less than min_len or it's non-space symbols is less than min_len
 * @return false
 */
bool CountVectorizer::skip_word(std::string token, size_t min_len)
{
  return (token.size() < min_len) ||
         std::all_of(token.begin(), token.end(), [](auto ch)
                     { return std::isspace(ch); }) ||
         std::count_if(token.begin(), token.end(), [](auto ch)
                       { return !std::isspace(ch); }) <
             min_len;
}

/**
 * @brief parse the source into tokens
 *
 * @return std::vector<std::string>
 */
std::vector<std::string> CountVectorizer::get_tokens()
{

  std::vector<std::string> tokens;

  std::string token;

  for (size_t i = 0; i < text.size(); i++)
  {

    switch (text[i])
    {
    case '\t':
    case '\n':
    case '\r':
    case ' ':
    case '#':
    case '.':
    case ',':
    case '!':
    case '(':
    case ')':
    case '[':
    case ']':
    case '{':
    case '}':
    case ';':
    case '\\':
    case '\'':
    case '\"':
    case '/':
    case '+':
    case '-':
    case ':':
    case '|':
    case '&':
    case '=':
    case '*':
    case '<':
    case '>':
      if (!token.empty())
      {
        tokens.emplace_back(token);
        token.clear();
      }
      break;
    default:
      token.push_back(text[i]);
    }
  }

  tokens.erase(std::remove_if(tokens.begin(), tokens.end(), [this](auto &tok)
                              { return skip_word(tok, 2); }),
               tokens.end());

  return tokens;
}

std::vector<std::string> CountVectorizer::get_ngrams(uint8_t grams)
{

  std::vector<std::string> ngrams;

  auto tokens = get_tokens();

  if (tokens.size() <= grams)
  {
    ngrams.push_back(std::accumulate(tokens.begin(), tokens.end(), std::string(""), [](const auto &t1, const auto &t2)
                                     { return t1 + " " + t2; }));
    return ngrams;
  }

  for (size_t i = 0; i < tokens.size() - grams + 1; i++)
  {

    std::string grams_str = std::accumulate(tokens.begin() + i, tokens.begin() + i + grams, std::string(""), [](const auto &t1, const auto &t2)
                                            { return t1 + " " + t2; });
    ngrams.emplace_back(grams_str);
  }

  std::for_each(ngrams.begin(), ngrams.end(), [](auto &tok)
                { tok.erase(tok.begin()); });

  return ngrams;
}

/**
 * @brief calculate occurances of tokens in source
 *
 * @return std::multimap<std::string, size_t>
 */
std::multimap<std::string, size_t> CountVectorizer::calculate_frequencies()
{

  std::multimap<std::string, size_t> freqs;

  std::vector<std::string> bigram_tokens = get_ngrams();

  for (const auto &tok : bigram_tokens)
  {
    if (freqs.find(tok) == freqs.end())
    {
      auto cnt = std::count(bigram_tokens.begin(), bigram_tokens.end(), tok);
      freqs.insert({tok, cnt});
    }
  }

  std::transform(freqs.begin(), freqs.end(), std::back_inserter(frequencies), [](const auto &freq)
                 { return freq.second; });

  return freqs;
}
/**
 * @brief getter of frequencies
 *
 * @return std::vector<size_t>
 */
std::vector<size_t> CountVectorizer::get_frequencies() const
{
  return frequencies;
}