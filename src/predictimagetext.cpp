#include <fstream>
#include <iostream>
#include "Logger.hpp"
#include "ImageReaderTesseract.hpp"
#include "OutputCorrecter.hpp"
#include "Preprocess.hpp"
#include "ImageReaderCustom.hpp"
#include "CharSegmenter.hpp"
#include "KNearestClassifier.hpp"
#include "RandomTreeClassifier.hpp"

bool DRAWROI = true;

bool test_Result(const std::string &result, const std::string &image_source)
{

  const std::source_location location{std::source_location::current()};

  std::ifstream res(result);
  std::istream_iterator<char> start_res(res), end_res;
  std::vector<char> result_chars(start_res, end_res);
  res.close();

  std::ifstream src(image_source);
  std::istream_iterator<char> start_src(src), end_src;
  std::vector<char> actual_chars(start_src, end_src);
  src.close();

  if (result_chars.size() != actual_chars.size())
  {
    log(location, "Sizes are different for %s:[%u] and %s:[%u]\n", result.c_str(), result_chars.size(), image_source.c_str(), actual_chars.size());
    /// return false;
  }
  size_t number_of_mismatches{};
  for (size_t i = 0; i < actual_chars.size(); i++)
  {
    log(location, "%c --- %c\n", actual_chars[i], result_chars[i]);
    if (actual_chars[i] != result_chars[i])
      number_of_mismatches++;
  }

  if (std::equal(result_chars.begin(), result_chars.end(), actual_chars.begin()))
  {
    log(location, "Files are the same.PASSED!!!\n");
    return true;
  }
  log(location, "%u%% mismatched\n", static_cast<unsigned>(static_cast<float>(number_of_mismatches) / actual_chars.size() * 100));

  auto m = std::mismatch(result_chars.begin(), result_chars.end(), actual_chars.begin());

  log(location, "Result :%c, Actual:%c\n", *m.first, *m.second);

  return false;
}

const std::string imgdata = "imgdata.csv";

int main(int argc, char *argv[])
{

  const std::source_location location{std::source_location::current()};
  std::string filename = "../test/data/code.png";

  if (argc > 1)
  {
    filename = argv[1];
    log(location, "Filename:%s\n", filename.c_str());
  }

  if (!std::filesystem::exists(std::filesystem::path(filename)))
  {
    log<LogLevel::INFO>(location, "Sample file does not exist: %s\n", filename.c_str());
    exit(EXIT_FAILURE);
  }

  std::string result_of_tesseract, result_of_knear, result_of_svm, result_of_dtress;
  std::filesystem::path result_of_tesseract_file, result_of_knear_file, result_of_svm_file, result_of_dtress_file;

  ImageReaderIf *tess = new ImageReaderTesseract({filename}, "ImageReaderTesseract.result");
  tess->image2code();
  result_of_tesseract = *tess;
  result_of_tesseract_file = *tess;
  delete tess;

  ImageReaderCustom *reader = new ImageReaderCustom({filename}, "KNearestClassifier.result");
  reader->setClassifier(new KNearestClassifier(imgdata));
  reader->image2code();
  result_of_knear = *reader;
  result_of_knear_file = *reader;
  delete reader;

  reader = new ImageReaderCustom({filename}, "SVM.result");
  reader->setClassifier(new SVMClassifier(imgdata));
  reader->image2code();
  result_of_svm = *reader;
  result_of_svm_file = *reader;
  delete reader;

  reader = new ImageReaderCustom({filename}, "RandomTree.result");
  reader->setClassifier(new RandomTreeClassifier(imgdata));
  reader->image2code();
  result_of_dtress = *reader;
  result_of_dtress_file = *reader;
  delete reader;

  log(location, "----------------------------------------------------Result for ImageReaderTesseract----------------------------------------------\n %s\n", result_of_tesseract.c_str());
  log(location, "----------------------------------------------------Result for KNearestClassifier------------------------------------------------\n %s\n", result_of_knear.c_str());
  if (test_Result("KNearestClassifier.result", "../test/data/Test_91_64.txt"))
    log(location, "Fine!!!\n");
  log(location, "----------------------------------------------------Result for SVM----------------------------------------------------------------\n %s\n", result_of_svm.c_str());
  if (test_Result("SVM.result", "../test/data/Test_91_64.txt"))
    log(location, "Fine!!!\n");
  log(location, "----------------------------------------------------Result for Random Tree--------------------------------------------------------\n %s\n", result_of_dtress.c_str());
  if (test_Result("RandomTree.result", "../test/data/Test_91_64.txt"))
    log(location, "Fine!!!\n");

  return 0;
}
