#include "ASCIIHashOfString.hpp"
/**
 * @brief output obj to os stream
 *
 * @param os
 * @param obj
 * @return std::ostream&
 */
std::ostream &operator<<(std::ostream &os, const ASCIIHashOfString &obj)
{

  os << obj.class_name << "\n {  ";
  std::for_each(obj.hash.begin(), obj.hash.end(), [&os](auto hash_val)
                { os << hash_val.first << " " << hash_val.second; });
  os << "}";

  return os;
}

/**
 * @brief get actual feature vector
 *
 * @return std::vector<size_t>
 */
std::vector<size_t> ASCIIHashOfString::get_hash() const
{

  std::vector<size_t> freqs;
  std::transform(hash.begin(), hash.end(), std::back_inserter(freqs), [](const auto &freq)
                 { return freq.second; });
  return freqs;
}
/**
 * @brief feature vector calculater
 *
 */
void ASCIIHashOfString::calculate_hash()
{

  const std::source_location location{std::source_location::current()};

  if (get_path().empty())
  {
    log<LogLevel::ERROR>(location, "The path %s is empty\n", get_path().string().c_str());
    return;
  }

  std::ifstream file(get_path().string());
  if (!file.is_open())
  {
    log<LogLevel::ERROR>(location, "The stream to %s could not be opened\nData won't be added\n", get_path().string().c_str());
    return;
  }

  std::string whole_file{std::istreambuf_iterator<char>(file),
                         std::istreambuf_iterator<char>()};

  file.close();
  log(location, "Whole File :{ %s \n}\n", whole_file.c_str());

  auto class_specifiers = lang_chars.find(static_cast<Classes>(HashIf::getClass(class_name)));

  if (class_specifiers != lang_chars.end())
  {
    StringPreProcess processor(whole_file);

    processor.setSingleLineCommentChar(std::get<0>(class_specifiers->second));
    processor.setBlockCommentChars(std::get<1>(class_specifiers->second));
    processor.setStringChars(std::get<2>(class_specifiers->second));

    std::string string_chars = "";
    std::for_each(std::get<2>(class_specifiers->second).begin(), std::get<2>(class_specifiers->second).end(), [&string_chars](const auto &symbol)
                  { string_chars += "  " + symbol; });
    log(location, "Single line comment:%s\nMultiple line comment : %s--%s\n String quote:%s\n", std::get<0>(class_specifiers->second).c_str(), std::get<1>(class_specifiers->second).first.c_str(), std::get<1>(class_specifiers->second).second.c_str(), string_chars.c_str());

    processor.remove_strings().remove_comments().remove_literals().remove_blank_lines();

    whole_file = processor.getContent();
  }
  else
  {
    log<LogLevel::WARNNING>(location, "String preprocessing is not possible for comments and strings %s!\n", get_path().string().c_str());
  }

  log(location, "Whole File after processing :{ %s \n}\n", whole_file.c_str());

  CountVectorizer vectorizer(whole_file);

  auto res = vectorizer.calculate_frequencies();

  hash.merge(res);
}

/**
 * @brief calculate feature vector of given sample source file
 *
 * @param sample_file
 * @return std::vector<uint64_t>
 */
std::vector<double> ASCIIHashOfString::calculate_hash_sample(std::string sample_file, std::string save_to, std::map<std::string, std::vector<double>> &freqs_of_langs)
{

  const std::source_location location{std::source_location::current()};

  std::map<std::string, std::set<std::string>> vocabularies = PrepareLangData::load_vocabulary(std::filesystem::path{save_to});

  std::filesystem::path smp{sample_file};
  std::vector<double> freqs;

  auto parent = std::filesystem::path{save_to}.parent_path().string();
  if (!parent.empty())
    parent += "/";

  if (vocabularies.empty())
  {
    log<LogLevel::ERROR>(location, "Could not load the vocabularies%s\n", (parent + PrepareLangData::vocabulary_file_prefix + "_${class}.data"));
    return freqs;
  }

  if (smp.empty())
  {
    log<LogLevel::ERROR>(location, "The path %s is empty\n", sample_file.c_str());
    return freqs;
  }

  std::ifstream file(smp.string());
  if (!file.is_open())
  {
    log<LogLevel::ERROR>(location, "The stream to %s could not be opened\n", sample_file.c_str());
    return freqs;
  }

  std::string whole_file{std::istreambuf_iterator<char>(file),
                         std::istreambuf_iterator<char>()};

  file.close();

  StringPreProcess processor(whole_file);
  processor.remove_blank_lines().remove_literals();
  whole_file = processor.getContent();
  log(location, "Whole File after processing :{ %s \n}\n", whole_file.c_str());

  CountVectorizer vectorizer(whole_file);

  auto res = vectorizer.calculate_frequencies();

  for (const auto voc : vocabularies)
  {
    size_t number_of_not_found{};
    std::vector<std::string> found_tokens;
    freqs.push_back(-1 * (HashIf::getClass(voc.first)));

    for (const auto &word : res)
    {
      const auto &it = voc.second.find(word.first);
      if (it == voc.second.end())
      {
        log<LogLevel::WARNNING>(location, "\"%s\" from sample file could not be found in vocabulary\n", word.first.c_str());
        number_of_not_found++;
        continue;
      }
      size_t token_index = voc.second.size() - std::distance(it, voc.second.end());

      freqs.push_back(freqs_of_langs[voc.first][token_index]);
      found_tokens.push_back(word.first);
    }

    log(location, "%u tokens not found , %u tokens found in %s\nFound Tokens : ", number_of_not_found, res.size() - number_of_not_found, (parent + PrepareLangData::vocabulary_file_prefix + "_" + voc.first + ".data").c_str());

    for (const auto &tok : found_tokens)
    {
      std::cout << tok << ",";
    }
    std::cout << std::endl;
  }

  std::cout << "\n---------------------------------------" << sample_file << "-------------------------------------------------\n";
  std::for_each(freqs.begin(), freqs.end(), [](const auto &p)
                { std::cout << p << "  "; });
  std::cout << "\n-----------------------------------------------------------------------------------------------------\n";

  return freqs;
};
