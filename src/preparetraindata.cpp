#include "PrepareImgData.hpp"
#include "PrepareLangData.hpp"
#include "Logger.hpp"
bool DRAWROI = false;

int main(int argc, char *argv[])
{

     const std::source_location location{std::source_location::current()};

     cv::String keys = "{help h usage ? |                | ./preparetraindata.exe -langsrc -langdst -imgsrc -imgdst -which}"
                       "{which          |  3             | 1 only to prepare imgdata ,2 only to prepare langdata,3 to prepare langdata and imgdata}"
                       "{langsrc        | ..\\langdata   | lang source}"
                       "{langdst        |  langdata.csv  | lang destination path}"
                       "{imgsrc         | ..\\imgdata    | image source}"
                       "{imgdst         |  imgdata.csv   | image destination path}"
                       "{show           |  false         | true to draw rois on training image}";
     cv::CommandLineParser parser(argc, argv, keys);
     if (parser.has("help"))
     {
          parser.printMessage();
          return 0;
     }

     if(parser.has("show")){
          DRAWROI = parser.get<bool>("show");
     } 

     if (parser.has("which"))
     {
          auto which = parser.get<int>("which");
          PrepareDataIf *pre;
          if (which == 1 || which == 3)
          {
               log(location, "Image reader training source:%s\n", parser.get<cv::String>("imgsrc").c_str());
               log(location, "Image reader training destination:%s\n", parser.get<cv::String>("imgdst").c_str());

               pre = new PrepareImgData(parser.get<cv::String>("imgsrc"), parser.get<cv::String>("imgdst"));
               pre->save_to_csv(true);
               delete pre;
          }
          if (which == 2 || which == 3)
          {
               log(location, "Language detection training source:%s\n", parser.get<cv::String>("langsrc").c_str());
               log(location, "Language detection training destination:%s\n", parser.get<cv::String>("langdst").c_str());

               pre = new PrepareLangData(parser.get<cv::String>("langsrc"), parser.get<cv::String>("langdst"));
               pre->save_to_csv(true);
               delete pre;
          }
     }

     return 0;
}