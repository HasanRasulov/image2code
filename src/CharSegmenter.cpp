#include "CharSegmenter.hpp"
/**
 * @brief acquire line images from source
 *
 * @return std::vector<cv::Mat>
 */
std::vector<cv::Mat> CharSegmenter::getLines()
{
    const std::source_location location{std::source_location::current()};

    auto line_segs = getLineSegments();

    log(location, "Number of clusters:%u\n", line_segs.size());

    std::vector<cv::Mat> res;
    if (line_segs.empty())
        return res;

    auto cluster = line_segs[0];
    auto baseline = cluster[1];
    cv::line(original, baseline.first, baseline.second, (255, 255, 255), 1);

    auto start_row = baseline.first.y;

    for (size_t i = 1; i < line_segs.size(); i++)
    {
        cluster = line_segs[i];
        baseline = cluster[1];

        // crop and push segment to res
        res.push_back(original(cv::Range(start_row, baseline.first.y), cv::Range(0, original.cols)));
        log(location, "(%d:%d)\n", start_row, baseline.first.y);
        cv::line(original, baseline.first, baseline.second, (255, 255, 255), 1);
        start_row = baseline.first.y;
    }

    cv::imwrite("Lines.png", original);
    return res;
}

/**
 * @brief to get black lines divides th lines
 *
 * @return std::vector<std::vector<std::pair<cv::Point, cv::Point>>>
 *   every element of ressult vector will have the positions of black lines between lines
 */
std::vector<std::vector<std::pair<cv::Point, cv::Point>>> CharSegmenter::getLineSegments()
{
    std::vector<std::pair<cv::Point, cv::Point>> cluster_of_lines;
    std::vector<std::vector<std::pair<cv::Point, cv::Point>>> all_lines;

    for (int i = 1, prev = 0; i < img.rows; i++)
    {
        std::vector<uint8_t> pixels;
        img.row(i).copyTo(pixels);

        if (Preprocess::isSameColourRow(pixels, img.cols, 1.f, Preprocess::BLACK.first))
        {
            if (prev + 1 == i)
            {
                cluster_of_lines.push_back(std::make_pair(cv::Point{0, i}, cv::Point{img.cols, i}));
                prev++;
                if (i == img.rows - 1)
                    all_lines.push_back(cluster_of_lines);
            }
            else
            {
                all_lines.push_back(cluster_of_lines);
                cluster_of_lines.clear();
                cluster_of_lines.push_back(std::make_pair(cv::Point{0, i}, cv::Point{img.cols, i}));
                prev = i;
            }
        }
    }

    return all_lines;
}
/**
 * @brief acquire words from lines
 *
 * @return std::vector<cv::Mat>
 */
std::vector<cv::Mat> CharSegmenter::getWords()
{
    auto lines = getLines();
    std::vector<cv::Mat> words;

    for (size_t img_idx = 0; img_idx < lines.size(); img_idx++)
    {
        auto potential_word = lines[img_idx];
        std::string line_name = "Words_" + std::to_string(img_idx + 1);

        cv::Mat potential_word_pre, gray;
        cv::cvtColor(potential_word, gray, cv::COLOR_BGR2GRAY);
        cv::threshold(gray, potential_word_pre, 0, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);

        auto word_segs = getWordSegments(potential_word_pre);

        if (word_segs.empty())
            continue;

        // remove seg size less than 4
        auto useless_segs = std::remove_if(word_segs.begin(), word_segs.end(), [](auto seg_vec)
                                           { return seg_vec.size() < 2; });
        word_segs.erase(useless_segs, word_segs.end());
        std::pair<cv::Point, cv::Point> start_col = word_segs[0][1];
        cv::line(potential_word, start_col.first, start_col.second, (255, 255, 255), 1);

        for (int possible_seg = 1; possible_seg < word_segs.size(); possible_seg++)
        {
            auto middle_black_line = word_segs[possible_seg][1];
            words.push_back(potential_word(cv::Range{0, potential_word.rows}, cv::Range{start_col.first.x, middle_black_line.first.x}));

            cv::line(potential_word, middle_black_line.first, middle_black_line.second, (255, 255, 255), 1);
            start_col = middle_black_line;
        }
    }

    if (cv::waitKey(0) == 27)
    {
        cv::destroyAllWindows();
    }

    return words;
}
/**
 * @brief get white lines seperates the words
 *
 * @param line_img
 * @return std::vector<std::vector<std::pair<cv::Point, cv::Point>>>
 */
std::vector<std::vector<std::pair<cv::Point, cv::Point>>> CharSegmenter::getWordSegments(cv::Mat line_img)
{
    const std::source_location location{std::source_location::current()};
    const float margin = 0.f;

    std::vector<std::pair<cv::Point, cv::Point>> cluster_of_words;
    std::vector<std::vector<std::pair<cv::Point, cv::Point>>> all_words;

    for (int i = 1, prev = 0; i < line_img.cols; i++)
    {
        std::vector<uint8_t> pixels;
        line_img.col(i).copyTo(pixels);

        if (Preprocess::isSameColourRow(pixels, line_img.rows, 1.f - margin))
        {
            if (prev + 1 == i)
            {
                cluster_of_words.push_back(std::make_pair(cv::Point{i, 0}, cv::Point{i, line_img.cols}));
                prev++;
                if (i == line_img.cols - 1)
                    all_words.push_back(cluster_of_words);
            }
            else
            {
                all_words.push_back(cluster_of_words);
                cluster_of_words.clear();
                cluster_of_words.push_back(std::make_pair(cv::Point{i, 0}, cv::Point{i, line_img.cols}));
                prev = i;
            }
        }
    }

    return all_words;
}
/**
 * @brief acquire chars from words
 *
 * @return std::vector<cv::Mat>
 */

std::vector<cv::Mat> CharSegmenter::getChars()
{
    const std::source_location location{std::source_location::current()};

    auto words = getWords();

    log(location, "%u number of wwords\n", words.size());

    std::vector<cv::Mat> chars;

    for (const auto &w : words)
    {
        cv::Mat gray, pre;
        cv::cvtColor(w, gray, cv::COLOR_BGR2GRAY);
        cv::threshold(gray, pre, 0, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);

        auto segs = getCharSegments(pre);
        log(location, "Number of clusters(potential charcters) :%u\n", segs.size());
        /*
          the cluster at the beginig and end of image should excluded
        */
        if (segs.size() - 2 >= 0)
        {
            /*
             the cluster at the beginig and end of image should be excluded
             So,it should find spaces in [begin+1,end-1]
            */
            if (std::any_of(segs.begin() + 1, segs.end() - 1, [](auto cluster)
                            { return cluster.size() > 3; }))
            {

                std::vector<std::vector<cv::Point>> contours;
                cv::findContours(pre, contours, cv::RETR_EXTERNAL,
                                 cv::CHAIN_APPROX_NONE);
                std::vector<cv::Rect> rois;
                std::transform(contours.begin(), contours.end(), std::back_inserter(rois), [](auto contour)
                               { return cv::boundingRect(contour); });

                if (rois.size() != 1)
                {
                    rois.erase(std::remove_if(rois.begin(), rois.end(), [&w](auto &roi)
                                              { return w.cols == roi.width && w.rows == roi.height; }),
                               rois.end());
                    std::sort(rois.begin(), rois.end(), [](auto r1, auto r2)
                              { return r1.x < r2.x; });

                    std::transform(rois.begin(), rois.end(), std::back_inserter(chars), [&w](const auto &ch_roi)
                                   { return w(ch_roi); });
                }
                else
                    chars.push_back(w);
            }
            else
                chars.push_back(w);
        }
        else
            chars.push_back(w);
    }

    std::vector<cv::Mat> cropped_chars;

    /*
      remove blank rows and columns
    */
    std::transform(chars.begin(), chars.end(), std::back_inserter(cropped_chars), [](const auto &ch)
                   {    
                    cv::Mat gray, pre;
                    cv::cvtColor(ch, gray, cv::COLOR_BGR2GRAY);
                    cv::threshold(gray, pre, 0, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);
                    return Preprocess::remove_begining_trailing_background(pre, 1, 255); });

    return cropped_chars;
}

/**
 * @brief get white rows divides the words
 *
 * @param word_img
 * @return std::vector<std::vector<std::pair<cv::Point, cv::Point>>>
 */
std::vector<std::vector<std::pair<cv::Point, cv::Point>>> CharSegmenter::getCharSegments(cv::Mat word_img)
{

    const std::source_location location{std::source_location::current()};
    const float margin = 0.03f;

    std::vector<std::pair<cv::Point, cv::Point>> cluster_of_chars;
    std::vector<std::vector<std::pair<cv::Point, cv::Point>>> all_chars;

    for (int i = 1, prev = 0; i < word_img.cols; i++)
    {
        std::vector<uint8_t> pixels;
        word_img.col(i).copyTo(pixels);

        if (Preprocess::isSameColourRow(pixels, word_img.rows, 1.f - margin))
        {
            if (prev + 1 == i)
            {
                cluster_of_chars.push_back(std::make_pair(cv::Point{i, 0}, cv::Point{i, word_img.cols}));
                prev++;
                if (i == word_img.cols - 1)
                    all_chars.push_back(cluster_of_chars);
            }
            else
            {
                all_chars.push_back(cluster_of_chars);
                cluster_of_chars.clear();
                cluster_of_chars.push_back(std::make_pair(cv::Point{i, 0}, cv::Point{i, word_img.cols}));
                prev = i;
            }
        }
    }

    return all_chars;
}