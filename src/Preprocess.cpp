#include "Preprocess.hpp"

/**
 * @brief preprocess the image
 *
 * @param img
 * @return cv::Mat
 */
cv::Mat Preprocess::preprocess(cv::Mat img)
{

  cv::Mat resized;

  /*if (img.cols < 300 || img.rows < 300)
  {

    cv::resize(img, resized, cv::Size(500, 500), cv::INTER_LINEAR);
  }
  else
  {*/
  img.copyTo(resized);
  //}

  cv::Mat gray_img;
  cv::cvtColor(resized, gray_img, cv::COLOR_BGR2GRAY);

  cv::Mat blured;
  cv::GaussianBlur(gray_img, blured, cv::Size(7, 7), 0, 0);
  // cv::imshow("Gaussian",blured);

  // cv::medianBlur(blur,med,3);
  // cv::blur(res, res, cv::Size(3, 3));*/

  cv::Mat threshold;
  cv::adaptiveThreshold(blured, threshold, 255,
                        cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY_INV, 11, 2); //??
  // cv::imshow("Threshold", threshold);

  cv::Mat eroded;
  cv::Mat kernel1 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3)); // 3*3
  cv::erode(threshold, eroded, kernel1, cv::Point(-1, -1), 1);

  cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(2, 2)); // 3*3 MORPH_RECT

  // unsigned char a[8][1] = {{0},{1},{1},{1},{1},{1},{1},{0}};
  // cv::Mat kernel(8,1,CV_8U,a);

  cv::Mat dilated;
  cv::dilate(eroded, dilated, kernel, cv::Point(-1, -1), 1); //  cv::dilate(threshold, dilated, kernel,cv::Point(-1,-1),1);//7

  // cv::imshow("Dilate",dilated);

  // cv::imshow("Erode",eroded);

  cv::Mat canny_output;
  cv::Canny(dilated, canny_output, 200, 200 * 2);

  return dilated;
}
/**
 * @brief to deskew given image
 *
 * @param img
 * @return cv::Mat
 */
cv::Mat Preprocess::deskew(cv::Mat &img)
{
  cv::Mat gray_img;
  cv::cvtColor(img, gray_img, cv::COLOR_BGR2GRAY);

  cv::Moments m = moments(gray_img);

  if (abs(m.mu02) < 1e-2)
  {

    return gray_img.clone();
  }

  auto SZ = 20;
  float skew = m.mu11 / m.mu02;

  cv::Mat warpMat = (cv::Mat_<float>(2, 3) << 1, skew, -0.5 * SZ * skew, 0, 1, 0);

  cv::Mat imgOut = cv::Mat::zeros(gray_img.rows, gray_img.cols, gray_img.type());

  warpAffine(gray_img, imgOut, warpMat, imgOut.size(), cv::WARP_INVERSE_MAP | cv::INTER_LINEAR);

  return imgOut;
}
/**
 * @brief resize the image HOG descriptor
 *
 * @param img
 * @return cv::Mat
 */
cv::Mat Preprocess::resize(const cv::Mat img)
{

  cv::Mat resized;

  cv::resize(img, resized, cv::Size(WIDTH_HOG, HEIGHT_HOG), cv::INTER_LINEAR);

  return resized;
}
/**
 * @brief to check whether all the elements of column or row has same colour value
 *
 * @param threshold the value to decide whether row or column is in colour or not
 * @param colour  actual grayscale colour
 * @return true   if it has the same colour
 * @return false
 */
bool Preprocess::isSameColourRow(const std::vector<uint8_t> &pixels, size_t max_of_pixels, float threshold, uint8_t colour)
{
  auto count_of_whites_in_row = std::count_if(pixels.begin(), pixels.end(), [&](auto pixel_val)
                                              { return pixel_val == colour; });

  float percentage = 0.f;

  if (std::abs(static_cast<long long>(max_of_pixels - count_of_whites_in_row)) < 2)
    percentage = 1.0f;
  else
    percentage = static_cast<float>(count_of_whites_in_row) / max_of_pixels;

  return percentage >= threshold;
}
/**
 * @brief function will remove the blank rows and columns from begining and end of image
 *
 * @param src  source image
 * @param threshold to decide the row or column satisfy the colour
 * @param colour colour of row or column to remove
 * @return cv::Mat result
 */
cv::Mat Preprocess::remove_begining_trailing_background(const cv::Mat &src, float threshold, uint8_t colour)
{
  const std::source_location location{std::source_location::current()};

  constexpr auto margin = 0.f;
  constexpr auto white_tolerance = 5u;
  size_t col_start{}, row_start{}, col_end = src.cols - 1, row_end = src.rows - 1;
  int i, j;
  std::vector<uint8_t> pixels;

  for (i = 1; i < src.rows; i++)
  {
    pixels.clear();
    src.row(i).copyTo(pixels);
    if (!isSameColourRow(pixels, src.cols, 1.f - margin))
    {
      row_start = i;
      break;
    }
  }

  for (j = 1; j < src.cols; j++)
  {
    pixels.clear();
    src.col(j).copyTo(pixels);

    if (!isSameColourRow(pixels, src.rows, 1.f - margin))
    {
      col_start = j;
      break;
    }
  }

  for (i = src.rows - 1; i >= 0; i--)
  {
    pixels.clear();
    src.row(i).copyTo(pixels);

    if (!isSameColourRow(pixels, src.cols, 1.f - margin))
    {
      row_end = i;
      break;
    }
  }

  for (j = src.cols - 1; j >= 0; j--)
  {
    pixels.clear();
    src.col(j).copyTo(pixels);

    if (!isSameColourRow(pixels, src.rows, 1.f - margin))
    {
      col_end = j;
      break;
    }
  }

  // log<LogLevel::INFO>(location, "Size(%u,%u)\n", src.rows, src.cols);
  // log<LogLevel::INFO>(location, "(%u,%u):(%u,%u)\n", row_start, row_end, col_start, col_end);
  row_end = row_end + white_tolerance < src.rows ? row_end + white_tolerance : src.rows;
  col_end = col_end + white_tolerance < src.cols ? col_end + white_tolerance : src.cols;
  return cv::Mat(src, cv::Range(row_start, row_end), cv::Range(col_start, col_end));
}