#include "BayesClassifier.hpp"

void BayesClassifier::train_and_test_classfier()
{
    const std::source_location location{std::source_location::current()};

    log(location, "Training the classifier ...\n");
    model = cv::ml::NormalBayesClassifier::create();
    model->train(train_data);

    cv::Mat resp;
    auto err = model->calcError(train_data, true, resp);
    log(location, "Error:%f\n", err);

    if (!filename_to_save.empty())
    {
        model->save(filename_to_save);
    }
}

float BayesClassifier::predict(const cv::Mat &sample)
{
    cv::Mat result(0, 0, CV_32FC1);

    model->predict(sample, result);

    auto res = result.at<float>(0, 0);
    return res;
}

BayesClassifier::BayesClassifier(std::string filename_to_read) : ClassifierIf{filename_to_read}
{
    const std::source_location location{std::source_location::current()};

    filename_to_save = get_classifier_name(location);
    filename_to_save += "_model.data";
}

std::pair<bool, size_t> BayesClassifier::load()
{

    model = cv::ml::NormalBayesClassifier::load(filename_to_save);

    return {false, 0};
}
BayesClassifier::~BayesClassifier() {}