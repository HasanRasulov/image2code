#include "CumulativeFrequencyAdditionClassifier.hpp"

void CumulativeFrequencyAdditionClassifier::train_and_test_classfier()
{
    const std::source_location location{std::source_location::current()};
    int number_of_correct_predictions{};
    // log number of files per class
    for (auto test_entry : test_entries)
    {
        auto actual_class = HashIf::getClass(test_entry.first.extension().string().substr(1));

        int predicted_class{};
        if (std::all_of(test_entry.second.begin(), test_entry.second.end(), [](auto h)
                        { return h <= 0.f; }))
        {
            log<LogLevel::ERROR>(location, "No word was found from %s in vocabularies\n", test_entry.first.string().c_str());
        }

        cv::Mat sample(1, test_entry.second.size(), CV_64FC1, test_entry.second.data());

        predicted_class = predict(sample);

        if (predicted_class == actual_class)
        {
            log(location, "Prediction is correct for %s (%s==%s)\n", test_entry.first.string().c_str(),HashIf::getClass(static_cast<Classes>(actual_class)).c_str(),HashIf::getClass(static_cast<Classes>(predicted_class)).c_str());
            number_of_correct_predictions++;
        }
        else
        {
            log(location, "Prediction is incorrect for %s (%s!=%s)\n", test_entry.first.string().c_str(),HashIf::getClass(static_cast<Classes>(actual_class)).c_str(),HashIf::getClass(static_cast<Classes>(predicted_class)).c_str());
        }
    }

    log(location, "The error : %f\n", static_cast<float>(number_of_test_files - number_of_correct_predictions) / number_of_test_files * 100.f);
}

size_t CumulativeFrequencyAdditionClassifier::prepare_train_and_test_data(const float unused)
{
    const std::source_location location{std::source_location::current()};
    
    if (path_to_test_sources.empty() || !std::filesystem::exists(path_to_test_sources))
    {
        log<LogLevel::ERROR>(location, "The path %s is empty or does not exist\n", path_to_test_sources.string().c_str());
        return 0;
    }

    auto freqs_sum = get_local_frequencies_accumulation();

    for (auto &dir_entry :
         std::filesystem::recursive_directory_iterator{path_to_test_sources})
    {
        if (dir_entry.is_regular_file())
        {
            number_of_test_files++;

            auto sample_hash = ASCIIHashOfString::calculate_hash_sample(dir_entry.path().string(), filename_to_read, freqs_sum);
            test_entries.emplace(std::filesystem::path{dir_entry}, sample_hash);
        }
    }
    log(location, "Number of test source files:%u\n", number_of_test_files);

    return number_of_test_files;
}
/**
 * @brief get the sum for each class and compare them
 *
 * @param sample
 * @return float Class of source code as float
 */
float CumulativeFrequencyAdditionClassifier::predict(const cv::Mat &sample)
{

    float result{};

    std::vector<double> freqs;

    sample.row(0).copyTo(freqs);

    std::vector<double>::iterator it, it1 = freqs.begin();
    double min_freq_sum = std::numeric_limits<double>::min();

    auto negative = [](const auto &e)
    { return e < 0; };

    while ((it = std::find_if(it1, freqs.end(), negative)) != freqs.end())
    {

        it1 = std::find_if(it + 1, freqs.end(), negative);

        double max = std::accumulate(it + 1, it1, 0.f);

        if (max > min_freq_sum)
        {
            min_freq_sum = max;
            result = *(it);
        }
    }

    return std::abs(result);
}

CumulativeFrequencyAdditionClassifier::CumulativeFrequencyAdditionClassifier(std::string filename_to_read) : ClassifierIf{filename_to_read},number_of_test_files{}
{
    const std::source_location location{std::source_location::current()};

    filename_to_save = get_classifier_name(location);
    filename_to_save += "_model.data";
}
/**
 * @brief read csv file without classes
 *
 * @param unused
 * @return std::pair<bool, size_t> unused
 */
std::pair<bool, size_t> CumulativeFrequencyAdditionClassifier::read_data(HashIf::converter unused)
{
    const std::source_location location{std::source_location::current()};

    for (const auto &cls : HashIf::lang_classes)
    {

        auto parent = std::filesystem::path{filename_to_read}.parent_path().string();
        if (!parent.empty())
            parent += "/";

        std::string file = parent + std::filesystem::path{filename_to_read}.stem().string() + "_"s + cls.second + std::filesystem::path{filename_to_read}.extension().string();
        std::ifstream data_stream(file);

        if (!data_stream.is_open())
        {
            log<LogLevel::ERROR>(location, "The stream could not be opened to %s", file.c_str());
            exit(0);
        }

        std::string entry;

        std::vector<std::vector<double>> entries_for_line;

        while (std::getline(data_stream, entry))
        {

            entry.erase(entry.begin(), std::find(entry.begin(), entry.end(), ',') + 1);
            auto splited_text = split(entry, ',');

            std::vector<double> data_vect;

            std::transform(splited_text.begin(), splited_text.end(),
                           std::back_inserter(data_vect),
                           [](const std::string &val) -> double
                           {
                               return std::stod(val);
                           });

            entries_for_line.emplace_back(data_vect);
            data_vect.clear();
        }
        entries.emplace(cls.second, entries_for_line);
        entries_for_line.clear();

        if (data_stream.is_open())
            data_stream.close();
    }

    return {true, 0};
}

std::map<std::string, std::vector<double>> CumulativeFrequencyAdditionClassifier::get_local_frequencies_accumulation()
{
    const std::source_location location{std::source_location::current()};

    std::map<std::string, std::vector<double>> result;

    for (auto &features : entries)
    {
        std::vector<double> freq_per_cls;
        freq_per_cls.resize(features.second[0].size());
        std::fill(freq_per_cls.begin(), freq_per_cls.end(), 0.f);

        for (size_t i = 0; i < features.second.size(); i++)
        {

            for (size_t j = 0; j < features.second[i].size(); j++)
            {
                freq_per_cls[j] += features.second[i][j];
            }
        }
        result.emplace(features.first, freq_per_cls);
    }

    return result;
}

std::pair<bool, size_t> CumulativeFrequencyAdditionClassifier::load()
{
    const std::source_location location{std::source_location::current()};

    log<LogLevel::WARNNING>(location, "Not implemented\n");
    return {false, 0};
}

CumulativeFrequencyAdditionClassifier::~CumulativeFrequencyAdditionClassifier() {}