#include "ImageReaderIf.hpp"
#include "Preprocess.hpp"

ImageReaderIf::ImageReaderIf(std::initializer_list<std::filesystem::path> paths, std::filesystem::path result)
    : path2imgs{paths}, path2result{result}
{
  result = "";
  for (size_t i = 0; i < path2imgs.size(); i++)
  {
    auto img = cv::imread(path2imgs[i].string(), cv::IMREAD_COLOR);
    auto res = Preprocess::preprocess(img);
    imgs.emplace_back(res);
  }
}

ImageReaderIf::operator std::filesystem::path()
{

  std::ofstream of(path2result);
  of << result;
  of.close();
  return path2result;
}

ImageReaderIf::operator std::string() { return result; }
