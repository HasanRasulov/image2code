#include "ImageReaderCustom.hpp"

void ImageReaderCustom::image2code()
{
    const std::source_location location{std::source_location::current()};
    std::pair<bool, size_t> number_of_variables;

    if (!std::filesystem::exists(std::filesystem::path(classifier->filename_to_save)))
    {
        log<LogLevel::INFO>(location, "Model has not been saved before: %s\n", classifier->filename_to_save.c_str());

        number_of_variables = classifier->read_data(static_cast<HashIf::converter>(RGBHashOfImg::getClass));

        if (!number_of_variables.first && number_of_variables.second == 0)
        {
            log<LogLevel::ERROR>(location, "Could not read the data!!\n");
            exit(EXIT_FAILURE);
        }

        classifier->prepare_train_and_test_data();

        classifier->train_and_test_classfier();
    }
    else
    {
        log(location, "Loading model:%s\n", classifier->filename_to_save.c_str());
        number_of_variables = classifier->load();
    }
    log(location, "Number of variables:%u\n", number_of_variables.second);

    for (const auto filename : path2imgs)
    {

        HOGHashOfImg hash(filename.string());
        auto hash_vals = hash.calculate_hash_sample();

        if (number_of_variables.first)
            for (auto &vec : hash_vals)
            {
                int diff = number_of_variables.second - vec.size();
                if (diff > 0)
                    vec.insert(vec.end(), diff, 0);
            }

        std::ostringstream output;

        log(location, "Number of characters:%u\n", hash.number_of_characters);

        for (size_t i = 0; i < hash.number_of_characters; i++)
        {
            cv::Mat sample(1, number_of_variables.second, CV_32FC1, hash_vals[i].data());
            auto rs = classifier->predict(sample);
            auto character = static_cast<char>(rs);
            output << character;
        }
        // log(location, "Result for %s\n:%s", filename.string().c_str(), output.str().c_str());
        result += output.str();

        output.str("");
    }
}

void ImageReaderCustom::setClassifier(ClassifierIf *clsfr)
{
    if (this->classifier)
        delete this->classifier;

    this->classifier = clsfr;
}