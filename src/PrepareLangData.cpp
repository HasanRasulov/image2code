#include "PrepareLangData.hpp"

PrepareLangData::PrepareLangData(std::filesystem::path path_to_data,
                                 std::filesystem::path save_to)
    : PrepareDataIf(path_to_data, save_to) {}

/**
 * @brief runs feature extraction on all the files from langdata
 * and save it to langdata.csv
 *
 * @param padding if it is true it will padd zeros to feature vectors to make them same length
 * @return uint16_t
 */
uint16_t PrepareLangData::save_to_csv(bool padding) const
{
  const std::source_location location{std::source_location::current()};

  std::map<std::string, std::filesystem::path> langdata_files;

  for (const auto &cls : HashIf::lang_classes)
  {
    auto f = save_to;
    auto file = f.replace_filename(f.stem().string() + "_" + cls.second).string() + f.extension().string();
    log<LogLevel::INFO>(location, "Data for %s in %s\n", cls.second.c_str(), file.c_str());
    langdata_files.insert({cls.second, file});
  }
  bool is_prepared = false;
  for (const auto &lang_file : langdata_files)
  {
    if (std::filesystem::exists(lang_file.second))
    {
      log<LogLevel::INFO>(location, "Data is prepared for %s in %s\n", lang_file.first.c_str(), lang_file.second.string().c_str());
      is_prepared = true;
    }
  }
  if (is_prepared)
    return 0;

  uint32_t number_of_files{};
  uint16_t number_of_classes{};

  for (auto &dir_entry : std::filesystem::recursive_directory_iterator{path})
  {
    if (dir_entry.is_regular_file())
      number_of_files++;

    else if (dir_entry.is_directory())
      number_of_classes++;
  }

  if (!number_of_files)
  {
    log<LogLevel::ERROR>(location, "No file to parse...!!!\n");
  }

  uint32_t number_of_threads =
      number_of_files > std::thread::hardware_concurrency()
          ? std::thread::hardware_concurrency()
          : number_of_files;

  log(location, "Number of classes: %u\nNumber of threads:%u Number of files:%u\n", number_of_classes, number_of_threads, number_of_files);

  std::vector<std::shared_ptr<ASCIIHashOfString>> results;

  {

    ThreadPool pool(number_of_threads);

    for (auto &dir_entry :
         std::filesystem::recursive_directory_iterator{path})
    {

      if (dir_entry.is_regular_file())
      {
        log(location, "Hash is being calculated for %s \n", dir_entry.path().string().c_str());
        auto result = std::make_shared<ASCIIHashOfString>(dir_entry.path());

        result->class_name = (--static_cast<std::filesystem::path>(dir_entry)
                                    .parent_path()
                                    .end())
                                 ->string();

        pool.submit(&HashIf::calculate_hash, result);

        results.emplace_back(result);
      }
    }
  }

  /**
   * @brief build vocabulary from found tokens in source files.
   * And count number of keyword per class
   */
  std::map<std::string, std::set<std::string>> vocabularies;
  std::map<std::string, size_t> counts;
  for (const auto &entry : results)
  {
    for (const auto &word : entry->hash)
    {
      counts[entry->class_name]++;
      vocabularies[entry->class_name].insert(word.first);
    }
  }

  std::map<std::string, std::ofstream> save_to_streams;
  for (auto &voc : vocabularies)
  {
    log(location, "The %s size for %s:%u\n", langdata_files[voc.first].string().c_str(), voc.first.c_str(), voc.second.size());
    save_vocabulary(voc);

    save_to_streams.emplace(voc.first, std::ofstream(langdata_files[voc.first]));
  }

  for (const auto &langdata : save_to_streams)
  {

    if (!langdata.second.is_open())
    {
      log<LogLevel::ERROR>(location, "The stream to %s could not be opened for %s vocabulary\n", langdata_files[langdata.first].c_str(), langdata.first.c_str());
      return -1;
    }
  }

  for (const auto &entry : results)
  {
    auto &vocabulary = vocabularies[entry->class_name];
    auto &stream = save_to_streams[entry->class_name];

    stream << entry->class_name << ",";

    std::vector<double> freqs;
    freqs.resize(vocabulary.size());
    std::fill(freqs.begin(), freqs.end(), 0.f);

    for (const auto &word : entry->hash)
    {
      const auto &it = vocabulary.find(word.first);
      if (it == vocabulary.end())
      {
        log(location, "Word was not found in vocabulary: \"%s\"\n", word.first.c_str()); // you should never see that.
        continue;
      }

      // calculate TF-IDF

      size_t number_of_files_contain_token{};

      for (const auto &entry : results)
      {
        if (entry->hash.contains(word.first))
          number_of_files_contain_token++;
      }

      // double tf = static_cast<double>(word.second) / vocabulary.size();
      double tf = std::log(static_cast<double>(word.second) + 1);
      auto idf = std::log(number_of_files / number_of_files_contain_token);

      auto tf_idf = tf * idf;

      size_t token_index = vocabulary.size() - std::distance(it, vocabulary.end());

      freqs[token_index] += tf_idf;
    }

    std::copy(freqs.begin(), freqs.end(),
              infix_ostream_iterator<double>(stream, ","));

    stream << std::endl;
  }

  for (auto &stream : save_to_streams)
    stream.second.close();

  std::for_each(counts.begin(), counts.end(), [location](const auto &cnt)
                { log(location, "There are %u number words for %s\n", cnt.second, cnt.first.c_str()); });

  return vocabularies.size();
}

/**
 * @brief save vocabulary to vocabulary_${class}.data
 *
 * @param voc pair of class and corresponding vocabulary set
 */
void PrepareLangData::save_vocabulary(const std::pair<std::string, std::set<std::string>> &voc) const
{

  const std::source_location location{std::source_location::current()};

  auto parent = save_to.parent_path().string();
  if (!parent.empty())
    parent += "/";

  std::string vocabulary_file = parent + vocabulary_file_prefix + "_" + voc.first + ".data";
  log(location,"Vocabulary for %s saved in %s : Number of tokens:%u \n",voc.first.c_str(),vocabulary_file.c_str(),voc.second.size());
  std::ofstream vocabulary(vocabulary_file);

  if (!vocabulary.is_open())
  {
    log<LogLevel::ERROR>(location, "The stream to %s could not be opened\n", vocabulary_file.c_str());
    return;
  }

  
  std::copy(voc.second.begin(), voc.second.end(),
            infix_ostream_iterator<std::string>(vocabulary, ","));
  vocabulary << std::flush;

  vocabulary.close();
}
/**
 * @brief loads vocabulary from vocabulary_${class}.data
 *
 * @return pairs of class and corresponding vocabulary set
 */
std::map<std::string, std::set<std::string>> PrepareLangData::load_vocabulary(std::filesystem::path save_to)
{
  const std::source_location location{std::source_location::current()};

  std::map<std::string, std::set<std::string>> res;

  for (const auto &class_name : HashIf::lang_classes)
  {
    std::set<std::string> voc;

    auto parent = save_to.parent_path().string();
    if (!parent.empty())
      parent += "/";

    std::string vocabulary_file = parent + vocabulary_file_prefix + "_" + class_name.second + ".data";

    if (!std::filesystem::exists(std::filesystem::path(vocabulary_file)))
    {
      log<LogLevel::WARNNING>(location, "Vocabulary does not exist: %s\n", vocabulary_file.c_str());
      return res;
    }

    std::ifstream vocabulary(vocabulary_file);

    if (!vocabulary.is_open())
    {
      log<LogLevel::ERROR>(location, "The stream to %s could not be opened\n", vocabulary_file.c_str());
      return res;
    }

    std::string word;
    while (std::getline(vocabulary, word, WORD_DELIM))
      voc.insert(word);

    vocabulary.close();
    res.emplace(class_name.second, voc);
  }
  return res;
}
