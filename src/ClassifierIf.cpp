#include "ClassifierIf.hpp"

/**
 * @brief split the string into strings
 *
 * @param str source
 * @param delimeter used to split the string
 * @return std::vector<std::string>
 */
std::vector<std::string> ClassifierIf::split(const std::string &str,
                                             char delimeter)
{

  std::vector<std::string> res;

  size_t first_pos = 0;

  while (true)
  {
    size_t second_pos = str.find(delimeter, first_pos);
    if (second_pos == std::string::npos)
    {
      res.push_back(str.substr(first_pos, str.size() - first_pos));
      break;
    }
    res.push_back(str.substr(first_pos, second_pos - first_pos));
    first_pos = second_pos + 1;
  };

  return res;
}
/**
 * @brief used to read the class from csv
 *
 * @param line
 * @return std::string::iterator
 */
std::string::iterator ClassifierIf::read_class_name(std::string &line)
{

  auto pos_of_first_comma = line.find(',');

  return std::next(line.begin(), pos_of_first_comma);
}

/**
 * @brief read data from csv
 *
 * @param class_name_converter convert the string version of class to integer.
 * @return std::pair<bool, size_t>
 */
std::pair<bool, size_t> ClassifierIf::read_data(
    HashIf::converter class_name_converter)
{

  std::ifstream data_stream(this->filename_to_read);
  const std::source_location location{std::source_location::current()};

  if (!data_stream.is_open())
  {
    log<LogLevel::ERROR>(location, "The stream could not be opened to %s", this->filename_to_read.c_str());
    return std::make_pair(false, 0);
  }

  std::string entry;
  size_t pos_of_del = 0;

  std::pair<bool, size_t> ispadded_and_number_of_vars = {};

  std::vector<size_t> sizes_of_vars;

  while (std::getline(data_stream, entry))
  {

    auto end_of_class_name = read_class_name(entry);
    std::string class_name(entry.begin(), end_of_class_name);
    std::string values(end_of_class_name + 1, entry.end());
    auto splited_text = split(values, ',');

    splited_text.erase(std::remove_if(splited_text.begin(), splited_text.end(), [](const auto &val)
                                      { return val.empty(); }),
                       splited_text.end());

    std::vector<float> data_vect;

    std::transform(splited_text.begin(), splited_text.end(),
                   std::back_inserter(data_vect),
                   [&location](const std::string &val) -> float
                   {
                     try
                     {
                       return std::stof(val);
                     }
                     catch (std::invalid_argument ex)
                     {
                       log(location, "Exception occured for value %s : %s\n", ex.what(), val.c_str());
                       return 0.0f;
                     }
                   });

    sizes_of_vars.push_back(data_vect.size());

    responses.push_back(class_name_converter(class_name));

    data.push_back(cv::Mat(1, data_vect.size(), CV_32F, data_vect.data()));
    data_vect.clear();
  }

  if (data_stream.is_open())
    data_stream.close();

  bool is_padded = std::equal(sizes_of_vars.begin(), sizes_of_vars.end(), sizes_of_vars.begin(), sizes_of_vars.end());
  if (is_padded)
  {
    log(location, "The data entry cols numbers %u\n", *sizes_of_vars.begin());
    return std::make_pair(is_padded, *sizes_of_vars.begin());
  }
  log(location, "The data entry cols numbers %u\n", *std::max(sizes_of_vars.begin(), sizes_of_vars.end()));
  return std::make_pair(is_padded, *std::max(sizes_of_vars.begin(), sizes_of_vars.end()));
}
/**
 * @brief prepare train and test data
 *
 * @param ratio to divide the training and test data
 * @return size_t
 */
size_t ClassifierIf::prepare_train_and_test_data(float ratio)
{
  const std::source_location location{std::source_location::current()};
  auto number_of_samples = data.rows;
  // check number of samples ..
  cv::Mat sample_idx = cv::Mat::zeros(1, number_of_samples, CV_8U);
  cv::Mat train_samples = sample_idx.colRange(0, static_cast<int>(number_of_samples * ratio));
  train_samples.setTo(cv::Scalar::all(1));
  log(location, "Number of columns:%u\n", data.cols);

  int nvars = data.cols;
  cv::Mat var_type(nvars + 1, 1, CV_8U);
  var_type.setTo(cv::Scalar::all(cv::ml::VAR_ORDERED));
  var_type.at<uchar>(nvars) = cv::ml::VAR_CATEGORICAL;

  train_data = cv::ml::TrainData::create(data, cv::ml::ROW_SAMPLE, responses,
                                         cv::noArray(), sample_idx, cv::noArray(), var_type);

  train_data->setTrainTestSplitRatio(ratio, true);
  train_data->shuffleTrainTest();
  return nvars;
}
/**
 * @brief used to get classifier name
 *
 * @param location
 * @return std::string
 */
std::string ClassifierIf::get_classifier_name(const std::source_location &location)
{

  std::string prototype{location.function_name()};
  auto pos_of_func_name = prototype.find(":");
  auto pos_of_class_beg = prototype.rfind(" ", pos_of_func_name) + 1;

  auto res = prototype.substr(pos_of_class_beg, pos_of_func_name - pos_of_class_beg);
  res += "_";
  res += std::filesystem::path{filename_to_read}.stem().string();
  return res;
}

ClassifierIf::ClassifierIf(std::string filename_to_read) : filename_to_read{filename_to_read}
{
}
ClassifierIf::~ClassifierIf()
{
}
