#include "SVMClassifier.hpp"

void SVMClassifier::train_and_test_classfier()
{
    const std::source_location location{std::source_location::current()};

    log(location, "Training the classifier ...\n");
    model = cv::ml::SVM::create();
    model->setType(cv::ml::SVM::C_SVC);
    model->setKernel(cv::ml::SVM::RBF);
    model->setC(3.125);
    model->setGamma(3.375);
    model->trainAuto(train_data);

    cv::Mat resp;
    auto err = model->calcError(train_data, true, resp);
    log(location, "Error:%f\n", err);

    if (!filename_to_save.empty())
    {
        model->save(filename_to_save);
    }
}

float SVMClassifier::predict(const cv::Mat &sample)
{
    cv::Mat result(0, 0, CV_32FC1);

    model->predict(sample, result);

    auto res = result.at<float>(0, 0);
    return res;
}
SVMClassifier::SVMClassifier(std::string filename_to_read) : ClassifierIf{filename_to_read}
{
    const std::source_location location{std::source_location::current()};

    filename_to_save = get_classifier_name(location);
    filename_to_save += "_model.data";
}

std::pair<bool, size_t> SVMClassifier::load()
{

    std::pair<bool, size_t> number_of_variables{true, 0u};
    std::ifstream mod(filename_to_save);

    std::string line;
    while (std::getline(mod, line))
    {

        if (line.find("var_count") != std::string::npos)
        {
            auto it = std::find_if(line.begin(), line.end(), [](char ch)
                                   { return isdigit(ch); });
            auto pos = std::distance(line.begin(), it);
            number_of_variables.second = atoi(line.substr(pos, line.length() - pos).c_str());
            break;
        }
    }

    mod.close();

    model = cv::ml::SVM::load(filename_to_save);

    return number_of_variables;
}
SVMClassifier::~SVMClassifier() {}