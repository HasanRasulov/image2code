#include "RandomTreeClassifier.hpp"

cv::TermCriteria RandomTreeClassifier::TC(int iters, double eps)
{
    return cv::TermCriteria(cv::TermCriteria::MAX_ITER + (eps > 0 ? cv::TermCriteria::EPS : 0), iters, eps);
}

void RandomTreeClassifier::train_and_test_classfier()
{
    const std::source_location location{std::source_location::current()};

    log(location, "Training the classifier ...\n");
    model = cv::ml::RTrees::create();
    model->setMaxDepth(10);
    model->setMinSampleCount(10);
    model->setRegressionAccuracy(0);
    model->setUseSurrogates(false);
    model->setMaxCategories(15);
    model->setPriors(cv::Mat());
    model->setCalculateVarImportance(true);
    model->setActiveVarCount(4);
    model->setTermCriteria(TC(100, 0.01f));
    model->train(train_data);

    cv::Mat resp;
    auto err = model->calcError(train_data, true, resp);
    log(location, "Error:%f\n", err);

    if (!filename_to_save.empty())
    {
        model->save(filename_to_save);
    }
}

float RandomTreeClassifier::predict(const cv::Mat &sample)
{
    cv::Mat result(0, 0, CV_32FC1);

    model->predict(sample, result);

    auto res = result.at<float>(0, 0);
    return res;
}

RandomTreeClassifier::RandomTreeClassifier(std::string filename_to_read) : ClassifierIf{filename_to_read}
{
    const std::source_location location{std::source_location::current()};

    filename_to_save = get_classifier_name(location);
    filename_to_save += "_model.data";
}

std::pair<bool, size_t> RandomTreeClassifier::load()
{

    model = cv::ml::RTrees::load(filename_to_save);
    return {false, 0};
}

RandomTreeClassifier::~RandomTreeClassifier() {}