#include "RGBHashOfImg.hpp"
#include <iostream>

/**
 * @brief Construct a new RGBHashOfImg::RGBHashOfImg object
 *
 * @param path
 */
RGBHashOfImg::RGBHashOfImg(std::filesystem::path path) : HashIf(path), number_of_characters{-1}
{
  const std::source_location location{std::source_location::current()};

  auto pos_first_underscore = path.string().find_first_of("_");
  auto pos_underscore = path.string().find_last_of("_");
  auto pos_dot = path.string().find_first_of(".");
  if (pos_first_underscore == std::string::npos || pos_underscore == std::string::npos)
  {
    max_width = max_height = 0;
    return;
  }

  max_width = atoi(path.string().substr(pos_first_underscore + 1, pos_underscore - pos_first_underscore).c_str());
  max_height = atoi(path.string().substr(pos_underscore + 1, pos_dot - pos_underscore).c_str());
  max_height += static_cast<size_t>(max_height * 0.2f);
  log(location, "Max height and width of character.Image: %s   MAX_Height:%u MAX_Width:%u\n", path.string().c_str(), max_height, max_width);
}
/**
 * @brief
 *
 * @param os
 * @param obj
 * @return std::ostream&
 */
std::ostream &operator<<(std::ostream &os, const RGBHashOfImg &obj)
{

  os << "{\n";
  os << "Path to file: " << obj.get_path().string() << "\n";
  for (const auto &img : obj.hash)
  {
    os << "Class: " << img.first << "\n";
    std::copy(img.second.begin(), img.second.end(),
              std::ostream_iterator<float>(std::cout, ","));
    os << "\n------------------------------------------------------------------"
          "---------------------\n";
  }
  os << "\n}\n";
  return os;
}
/**
 * @brief
 *
 * @param class_name
 * @return int
 */
int RGBHashOfImg::getClass(const std::string &class_name)
{

  return class_name.empty() ? -1 : static_cast<int>(class_name[0]);
}

/**
 * @brief
 *
 * @param max_number_of_vars
 */
void RGBHashOfImg::add_padding(size_t max_number_of_vars)
{
  const std::source_location location{std::source_location::current()};

  const auto &max_of_img = std::max_element(hash.begin(), hash.end(), [](const auto &entry1, const auto &entry2)
                                            { return entry1.second.size() < entry2.second.size(); });

  log(location, "At most %u number of padding will added for %s\n", max_number_of_vars - max_of_img->second.size(), get_path().string().c_str());

  std::for_each(hash.begin(), hash.end(),
                [max_number_of_vars](auto &data_entry)
                {
                  int diff = max_number_of_vars - data_entry.second.size();

                  if (diff > 0)
                    data_entry.second.insert(data_entry.second.end(), diff,
                                             0);
                });
}

/**
 * @brief
 *
 */
void RGBHashOfImg::calculate_hash()
{
  const std::source_location location{std::source_location::current()};

  if (!std::filesystem::exists(get_path()))
  {
    log<LogLevel::ERROR>(location, "File does not exists:%s", get_path().string().c_str());
    return;
  }

  cv::Mat img = cv::imread(get_path().string());
  if (DRAWROI)
    cv::imshow("Preprocess", img);

  cv::Mat edge_img = Preprocess::preprocess(img);

  std::vector<std::vector<cv::Point>> contours;
  cv::findContours(edge_img, contours, cv::RETR_EXTERNAL,
                   cv::CHAIN_APPROX_NONE);

  std::vector<cv::Rect> rois = getRoisForCharacters(contours, std::bind(&HashIf::is_near_rects, this, std::placeholders::_1, std::placeholders::_2));

  // std::transform(contours.begin(), contours.end(), std::back_inserter(rois), [](auto contour)
  //              { return cv::boundingRect(contour); });

  // std::for_each(rois.begin(),rois.end(),[this](auto r){std::cout<<max_width<<":"<<r.width << "   " <<max_height<<":"<<r.height<<"\n";});

  log(location, "%s : Reduced number of rois : %u\tNumber of contours : %u\n", get_path().string().c_str(), rois.size(), contours.size());
  // std::cout << rois.size()<< "---"<<contours.size()<<"\n";
  sort_contours(rois);

  this->number_of_characters = rois.size();

  if (DRAWROI)
    draw_rois(img, rois);

  if (number_of_characters != RGBHashOfImg::classes.size())
  {
    log<LogLevel::ERROR>(location, "[Error:%s] Wrong number of rois.Expect:%u Actual: %u Ignore that if you predict!!\n", get_path().string().c_str(), HashIf::classes.size(), rois.size());
    // if (max_width || max_height) TODO:if you stick to the method for training uncommne
    return;
  }

  std::vector<float> rgb_values;
  for (size_t i = 0; i < number_of_characters; i++)
  {
    cv::Mat character_img = edge_img(rois[i]);
    cv::Mat character_img_resized;
    cv::resize(character_img, character_img_resized,
               cv::Size(rois[i].width, rois[i].height));
    cv::Mat character_img_float;
    character_img_resized.convertTo(character_img_float, CV_32FC1);
    cv::Mat character_img_flattened = character_img_float.reshape(1, 1);

    rgb_values.clear();
    character_img_flattened.row(0).copyTo(rgb_values);
    auto data_entry = std::make_pair(RGBHashOfImg::classes[i], rgb_values);
    hash.emplace_back(data_entry);
  }
}
/**
 * @brief
 *
 * @return std::vector<std::vector<float>>
 */
std::vector<std::vector<float>> RGBHashOfImg::calculate_hash_sample()
{

  std::vector<std::vector<float>> hash;

  const std::source_location location{std::source_location::current()};

  if (!std::filesystem::exists(get_path()))
  {
    log<LogLevel::ERROR>(location, "File does not exists:%s", get_path().string().c_str());
    return hash;
  }
  cv::Mat img = cv::imread(get_path().string());
  if (DRAWROI)
    cv::imshow("Preprocess", img);

  cv::Mat edge_img = Preprocess::preprocess(img);

  std::vector<std::vector<cv::Point>> contours;
  cv::findContours(edge_img, contours, cv::RETR_EXTERNAL,
                   cv::CHAIN_APPROX_NONE);

  std::vector<cv::Rect> rois = getRoisForCharacters(contours, std::bind(&HashIf::is_inside, this, std::placeholders::_1, std::placeholders::_2));

  log(location, "%s : Reduced number of rois : %u\tNumber of contours : %u\n", get_path().string().c_str(), rois.size(), contours.size());

  sort_contours(rois);

  this->number_of_characters = rois.size();

  if (DRAWROI)
    draw_rois(img, rois);

  std::vector<float> rgb_values;
  for (size_t i = 0; i < number_of_characters; i++)
  {
    cv::Mat character_img = edge_img(rois[i]);
    cv::Mat character_img_resized;
    cv::resize(character_img, character_img_resized,
               cv::Size(rois[i].width, rois[i].height));
    cv::Mat character_img_float;
    character_img_resized.convertTo(character_img_float, CV_32FC1);
    cv::Mat character_img_flattened = character_img_float.reshape(1, 1);

    rgb_values.clear();
    character_img_flattened.row(0).copyTo(rgb_values);
    hash.push_back(rgb_values);
  }

  return hash;
}
