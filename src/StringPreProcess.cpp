#include "StringPreProcess.hpp"
/**
 * @brief Construct a new String Pre Process:: String Pre Process object
 * @warning This constructor is for only testing purpose
 * @param path_to_source
 */
StringPreProcess::StringPreProcess(std::filesystem::path path_to_source)
{

  const std::source_location location{std::source_location::current()};

  std::ifstream file(path_to_source.string());

  if (!file.is_open())
  {
    log<LogLevel::ERROR>(location, "The stream to %s could not be opened\n", path_to_source.string().c_str());
    return;
  }

  std::string whole_file{std::istreambuf_iterator<char>(file),
                         std::istreambuf_iterator<char>()};

  file.close();

  content = std::move(whole_file);
}
/**
 * @brief set block comment symbols
 *
 * @param chars
 */
void StringPreProcess::setBlockCommentChars(std::pair<std::string, std::string> chars)
{

  if (chars.first.size() != chars.second.size())
    throw std::invalid_argument("Start and end token should be same size!!!");

  block_comment_chars.first = chars.first;
  block_comment_chars.second = chars.second;
}
/**
 * @brief set single line comment symbol
 *
 * @param start
 */
void StringPreProcess::setSingleLineCommentChar(std::string start)
{

  single_line_comment_char = start;
}
/**
 * @brief set string symbols
 *
 * @param chars
 */
void StringPreProcess::setStringChars(std::vector<std::string> chars)
{

  string_chars = chars;
}
/**
 * @brief remove the language comments
 *
 * @return StringPreProcess&
 */
StringPreProcess &StringPreProcess::remove_comments()
{

  remove_chars_between(single_line_comment_char, std::string{newLine});

  remove_chars_between(block_comment_chars.first, block_comment_chars.second);

  return *this;
}
/**
 * @brief remove the strings from source
 *
 * @return StringPreProcess&
 */
StringPreProcess &StringPreProcess::remove_strings()
{

  for (const auto &string_char : string_chars)
  {
    remove_chars_between(string_char, string_char);
  }
  return *this;
}
/**
 * @brief find search string in str begining from begin
 *
 * @param str
 * @param search
 * @param begin
 * @return std::string::const_iterator to the start of search
 */
std::string::const_iterator
StringPreProcess::find_str(const std::string &str, const std::string &search,
                           std::string::const_iterator begin)
{

  size_t pos;
  if ((pos = str.find(search, std::distance(str.begin(), begin))) !=
      std::string::npos)
    return str.begin() + pos;

  return str.end();
}
/**
 * @brief it will remove the characters between start and end
 *
 * @param start
 * @param end
 */
void StringPreProcess::remove_chars_between(const std::string &start,
                                            const std::string &end)
{
  if (start.empty() || end.empty())
    return;

  std::string::const_iterator end_it = content.begin();

  while (true)
  {

    const auto &start_it = find_str(content, start, end_it);

    if (start_it == content.end())
      return;

    end_it = find_str(content, end, start_it + start.size());

    if (end_it == content.end())
      return;

    end_it = content.erase(start_it, end_it + end.size());
  }
}
/**
 * @brief remove blank lines
 *
 * @return StringPreProcess&
 */
StringPreProcess &StringPreProcess::remove_blank_lines()
{
  content.erase(std::remove_if(content.begin(), content.end(), [](auto ch)
                               { return ch == newLine[0]; }),
                content.end());

  return *this;
}
/**
 * @brief remove punctuations
 *
 * @return StringPreProcess&
 */
StringPreProcess &StringPreProcess::remove_punctuations()
{

  content.erase(std::remove_if(content.begin(), content.end(), [](auto ch)
                               { return std::ispunct(ch); }),
                content.end());

  return *this;
}
/**
 * @brief remove number literals
 *
 * @return StringPreProcess&
 */
StringPreProcess &StringPreProcess::remove_literals()
{

  content.erase(std::remove_if(content.begin(), content.end(), [](auto ch)
                               { return std::isdigit(ch); }),
                content.end());

  return *this;
}